(function (global) {

    // Uncomment to see trace from framework in console
    //debug();
    window.localisation = new Localisation();
    var query_string = window.location.href;
    query_string = query_string.split('=');

    var query_string_value = query_string[1];
    var logLength = localStorage.length;
    var topmenuarray = [];

    //GET LOCAL STORAGE
    var spaces = new RegExp('%20', "i");
    if (query_string_value != null) {
        if (query_string_value.match(spaces) != null) {
            query_string_value = query_string_value.replace(/%20/g, ' ');
        }
    }

    var appType = "db";
    var dynamicJson = ''
    var values;
    var slidesarray;
    var menuvalues;
    var menu; ;
    if (query_string_value != null && query_string_value != "") {
        if (query_string_value == "pfp123") {
            values = selectionsarray;
            slidesarray = values;
            menu = menuvalues = chapterarray;
        }
        else {
            //Get the local storage item and 
            values = localStorage.getItem(query_string_value);
            slidesarray = JSON.parse(values);
            menuvalues = localStorage.getItem(query_string_value + 'menu');
            menu = menuvalues.split(',');
        }
    }
    else {
        //        values = selectionsarray;
        //        slidesarray = values;
        //        menu = menuvalues = chapterarray;        
        var t = setTimeout("window.location='./_ASK_Extensions/Builder/index2.html'", redirectTimeout);
        return false;
        //        window.location = "./_ASK_Extensions/Builder/index.html";
        //        values = selectionsarray;
        //        slidesarray = values;
        //        menu = menuvalues = chapterarray;
    }

    for (i = 0; i < menu.length; i++) {
        if (menu[i].substring(0, 1) == '+' || menu[i].substring(0, 1) == '-') {
            menu[i] = menu[i].substring(1);
        }
    }

    var contentSlides = '';
    dynamicJson = '{\n"structures": {';

    for (i = 0; i < menu.length; i++) {
        dynamicJson += '\n"' + menu[i] + '": {';
        contentSlides += '"' + menu[i] + '",';
        dynamicJson += '\n"type": "slideshow",';
        dynamicJson += '\n"name": "' + menu[i] + '",';
        var content = '';
        try {
            var slideitem = slidesarray[i][0];
            if (slidesarray[i].length > 0) {
                for (var m = 0; m < slidesarray[i].length; m++) {
                    slideitem = slidesarray[i][m];
                    content += '"' + slideitem + '",';
                }
            }
            content = content.substring(0, content.length - 1);
        }
        catch (e) { }

        dynamicJson += '\n"content": [' + content + '],';
        dynamicJson += '\n"valid": true';

        dynamicJson += '\n},';
    }
    contentSlides = contentSlides.substring(0, contentSlides.length - 1);
    dynamicJson += '\n "presentation": {';
    dynamicJson += '\n"type": "collection",';
    dynamicJson += '\n"name": "Presentation",';
    dynamicJson += '\n"content": [' + contentSlides + '],';
    dynamicJson += '\n"valid": true';
    dynamicJson += '\n}\n},\n"storyboard": ["presentation"],';
    dynamicJson += '\n"slides": {';

    for (i = 0; i < menu.length; i++) {
        try {
            var slideitem = slidesarray[i][0];
            if (slidesarray[i].length > 0) {
                for (var m = 0; m < slidesarray[i].length; m++) {
                    dynamicJson += '\n"' + slidesarray[i][m] + '": {';
                    dynamicJson += '\n"name": "' + slidesarray[i][m] + '",';
                    dynamicJson += '\n"file": "' + slidesarray[i][m] + '.html",';
                    dynamicJson += '\n"valid": true';
                    dynamicJson += '\n},';
                }
            }
        }
        catch (e) { }
    }
    dynamicJson = dynamicJson.substring(0, dynamicJson.length - 1);
    dynamicJson += '\n}';
    dynamicJson += '\n}';
    console.log(dynamicJson);



    // Creating our presentation and global namespace "app"
    global.app = new Presentation({
        globalElements: ['mainmenu', 'mainfooter', 'slidePopup', 'popupBackButton'],
        type: appType,
        dynamicJson: dynamicJson,
        manageMemory: true
    });

    // Initiate modules
    app.scroller = new Slidescroller();
    app.data = new Data();
    app.loader = new Loader({ delay: 1600 });
    app.slidePopup = new SlidePopup('slidePopup');
    app.menu = new AutoMenu({
        offset: -482,
        links: {
            //      summary:      {title: 'Conclusion'},
            //      introduction: {title: ' ', classname: 'home'}
        }


    });


    // References Lightbox
    $('#lightboxOverlay').hide();
    $('.lightboxRef').hide();
    $('#closeRefs').hide();
    $("#openRefs").click(function () {
        var ref_height = $('.lightboxRef').outerHeight() / 2;
        console.log(ref_height);
        app.scroller.disableAll();
        $('#lightboxOverlay').fadeIn(600);
        $('.lightboxRef').css('margin-top', '-' + ref_height + 'px').fadeIn(600);
        $('#closeRefs').fadeIn(600);
    });
    $("#closeRefs").click(function () {
        $('#lightboxOverlay').fadeOut(600);
        app.scroller.enableAll();
        $('.lightboxRef').fadeOut(600);
        $('#closeRefs').fadeOut(600);
    });



    $('#popup-wrap #closePopup').click(function () {
        $('#lightboxOverlay').fadeOut(600);
        $(this).parent().fadeOut(600, function () {
            $(this).find('.popup-box').remove();
            $(this).removeAttr('style');
            app.scroller.enableAll();
        });

    });

    $('#popup-refs #closeRefs').click(function () {
        $('#lightboxOverlay').fadeOut(600);
        $(this).parent().fadeOut(600, function () {
            $(this).find('.popup-box').remove();
            $(this).removeAttr('style');
            app.scroller.enableAll();
        });

    });


    // Initialize presentation
    app.init('presentation');



})(window);


// Prevent vertical bouncing of slides
document.ontouchmove = function(e) {
   e.preventDefault();
};



function removeDuplicates() {
    var $duplicate = $('#thumbs').find('li.active:gt(0)');
    if ($duplicate.length) {
        var $duplicate_no = $duplicate.index() - 1;
        $('#thumbs').find('li:gt(' + $duplicate_no + ')').remove();
        $('.indicators').find('.indicator:gt(' + $duplicate_no + ')').remove();
    }
}
