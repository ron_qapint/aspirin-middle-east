var Utils = (function(){

       this.newElement = function(element,name,parent,attributes,attributename) {
         
          this.name = name;
          this.parent = parent;
          this.attributes = attributes;
          this.element = element;
          this.attributename;
          
          this.name = document.createElement(this.element);
          this.name.setAttribute(this.attributes, this.attributename);
          console.log('Utils: new element ' + this.element)
          this.parent.appendChild(name);
          
          return this.elementname 	        
        };
        this.include = function (file) {
        
            var script  = document.createElement('script');
            script.src  = file + '.js';
            script.type = 'text/javascript';
            //script.defer = true;
            document.getElementsByTagName('head').item(0).appendChild(script);
        };
        this.includeJS = function (file) {
        
            var script  = document.createElement('script');
            script.src  = jspath + file + '.js';
            script.type = 'text/javascript';
            //script.defer = true;
            document.getElementsByTagName('head').item(0).appendChild(script);
        };
        
        this.includeCSS = function(file) {
            var link  = document.createElement('link');
            link.href   =  csspath + file + '.css';
            link.rel    = 'stylesheet'
            link.type = 'text/css';
            link.charset= 'utf-8';
            //script.defer = true;
            document.getElementsByTagName('head').item(0).appendChild(link);
        };
        
        this.addClass = function(element,className) {
          if (element==undefined) {return; };
        	if (element.className.indexOf(className) <= -1) {
        		element.className += " " + className;
        	}
        };
        
        this.removeClass = function(element,className) {
        	if (element.className.indexOf(" " + className) > -1) {
        		element.className = element.className.replace(new RegExp(" "+className+"\\b"), "");
        	}
        	else if (element.className.indexOf(className) > -1) {
        		element.className = element.className.replace(new RegExp(className+"\\b"), "");
        	}
        };
        
        this.toArray = function(obj) { 
            return [].slice.call(obj, 0); 
        };        
          
});