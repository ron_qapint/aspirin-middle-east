var SlidesController = (function () {

    var homeset = false;
    var sectionset = false;
    var sections;
    var homeslide;
    var homeslideNr;
    var array;

    function setvars() {
        homeslide = model.sections[model.startslide];
        sections = utils.toArray(model.sections);
        homeslideNr = sections.indexOf(homeslide);
    }
    this.markActive = function () {
 
        if (model.openthumbs == false) {
            model.openthumbs = true;
        }
        if (sectionset == false) {
            setvars();
            sectionset = true;
        }

        this.currentSlide = model.currentSlide;
        var monitorId = this.currentSlide.getAttribute('data-monitorid');

        var array = utils.toArray(model.menuItems);
        var nextItem = array.indexOf(model.currentMenuItem) + 1;
        var prevItem = array.indexOf(model.currentMenuItem) - 1;
        var lastitem = array.length - 1;
        var sectionNr = sections.indexOf(model.currentSection);

        if (model.menuItems != undefined && model.menuItems != null) {
            var homebutton = model.menuItems[model.startslide];
            //model.navdrag.customset('0,0'); 

            document.getElementById('debugdiv').innerHTML = 'model.navdrag.dragging ' + model.navdrag.dragging + ' model.navdrag.position ' + model.navdrag.position;

            if (sectionNr > homeslideNr && homeset == false) {
                model.navScroller.style.cssText = '-webkit-transform:translate3d(' + (-homebutton.offsetLeft + 5) + 'px,0px,0px);';
                homeset = true;
            }
            if (sectionNr < homeslideNr && model.navdrag.dragging == false && homeset == true) {
                model.navScroller.style.cssText = '-webkit-transform:translate3d(' + (model.menuItems[lastitem].offsetLeft + model.menuItems[lastitem].width) + 'px,0px,0px);';
                homeset = false;
            }
            //document.getElementById('debugdiv').innerHTML = 'position ' + model.navScroller.childNodes[0].offsetLeft;
        }
        utils.addClass(model.currentSlide, 'active');

        /*console.log("Submit slide enter " + model.currentSlide.id + " "+ monitorId);*/
        if (model.monitoring === true) {
            submitSlideEnter(
	    	  				model.currentSlide.getAttribute('data-monitorid'),
	    					model.currentSlide.getAttribute('data-monitorname'),
	    	     			model.currentSlideIndex,
	    					model.currentSection.getAttribute('data-monitorid'),
	    	     		    "VTEp dynamic agenda - Global"
            //model.currentSection.id
	    	     		)
            callmonitor();
        }
        if (model.extensions === true) {
            extensionsHandler();
        }
    }
});