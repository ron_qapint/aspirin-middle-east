var App = (function(){
     
    var navigation = new Navigation();
    var footer = new Footer();
    var thumbnavigation = new ThumbNavigation();
    var extensions;
    	  
    this.__defineGetter__("dynamic",function() { return dynamic});
    this.__defineSetter__("dynamic",function(val) {dynamic=val;});
    
    this.init = function() {
          //SET WHICH NUMBER SLIDE TO START AT(NORMALLY THE HOME SLIDE)
          var startslide = 3;
          model.startslide = startslide;	 
          //TURN ON EXTENSIONS FOR CONTENT CONTROL
          extensions = true;
          model.extensions = extensions;
          //TURN ON MONITORING
          monitoringEnabled = false;
          model.monitoring = monitoringEnabled;
          model.addEventListener("slidesready", setviews, false);
          //CALLED BY START ANIMATION WHEN FINISHED
          model.addEventListener("startpresentation", initpresentation, false);
          setTimeout(loader, 1500);

      }
      
     function loader () {
         document.getElementById('loader').style.display = 'none';
         navigation.init = true; 
     }
       function setviews() {
             //TELL THE SLIDE SCROLLER TO GO TO HOME
              model.slidescroller.scrollTo(model.startslide,0);
             //LOAD THE JAVASCRIPT FILES FOR THE CONTENT
             for(var i = 0; i<jsarray.length; i++) {
                      utils.includeJS(jsarray[i]);
                      //console.log('Load js ' + jsarray[i]);
             }
             utils.include(jspath + 'popupWindow');
    }
	function initpresentation() {
         if (model.opennavigation != true) {
                 console.log('intro called ');
                 thumbnavigation.init = true;
                 footer.init = true;
                 model.opennavigation = true;
             
                 model.currentSectionIndex = model.startslide;
                 model.currentMenuItem = model.startslide;            
         }  
	}
	function opennav() {
	            model.opennavigation = true;
	}  
 });