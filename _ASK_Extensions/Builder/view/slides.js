//INIT CODE
var SlideShow = (function() {

var slidescontroller = new SlidesController();

var init = false;

var sectionarray = new Array();
var contentarray = new Array();
var slidesarray = model.slidesarray;
var slidenames = new Array();
var slides;
//console.log('slides ' + slidesarray)
this.__defineGetter__("init",function() { return init});
this.__defineSetter__("init",function(val) {
                     init=val; 
                  
                     if(init==false) { 
                        return
                        }
                     if(init==true) {
                        initslideloader();
                        }
                        
});

function initslideloader() {
    
    var filename;
    var primaryscroller = document.getElementById('scroller');
 
    for (var col = 0; col < slidesarray.length; col++) {
                    
                var newsection = document.createElement('section');
                newsection.setAttribute('class', 'slide');
                newsection.setAttribute('id', slidesarray[col][0]+'_section');
                newsection.setAttribute('data-monitorid', slidesidsarray[col][0]);
                primaryscroller.appendChild(newsection);	
                sectionarray.push(newsection); 
                
                if(slidesarray[col].length > 1) {
                       
                       for(var i = 0; i< slidesarray[col].length; i++) { 
                           var newslide = document.createElement('article');
                           newslide.setAttribute('id', slidesarray[col][i]);
                           newslide.setAttribute('data-monitorid', slidesidsarray[col][i]);
                           newslide.setAttribute('data-monitorname', slidesnamesarray[col][i]);
                           sectionarray[col].appendChild(newslide);
                           //console.log('Make many slide sections ' + slidesarray[col].id)
                           slidenames.push(newslide.id);
                           if(newslide == sectionarray[col].firstChild) {
                              newslide.setAttribute('class', 'innerslide morebelow');
                           } 
                           else if (i == (slidesarray[col].length-1)) {
                              newslide.setAttribute('class', 'innerslide moreabove');
                           } else {
                              newslide.setAttribute('class', 'innerslide morebelow moreabove');
                           }
                        }
                }
                    
                 if(slidesarray[col].length == 1) {
                         //console.log('Make single slide sections ' + sectionarray[col].id)
                         var newslide = document.createElement('article');
                         newslide.setAttribute('id', slidesarray[col][0]);
                         //newslide.setAttribute('data-monitorId', slidesidsarray[col][0]);
                         newslide.setAttribute('data-monitorname', slidesnamesarray[col][0]);
                         sectionarray[col].appendChild(newslide);
                         slidenames.push(newslide.id);
                 }
  
          }//End for loop
       getcontent();
 }
function getcontent() {

  var filename;
  for (var col = 0; col < slidesarray.length; col++) {
       var req = new XMLHttpRequest();
       
       for(var i = 0; i < slidesarray[col].length; i++) { 
            filename = slidesarray[col][i];
            setTimeout( construct(filename), 200);
       }  
         
 function construct(filename) {
          req.open('GET', path + filename + '.html', false)
          req.setRequestHeader("Content-type", "text/html");
          req.send(null);
      
          if(req.readyState == 4 && req.status == 0 || req.readyState == 4 && req.status == 200){
     	      content = req.responseText;
     	      contentarray.push(content);
     }              	                                
    }
    }
        initscroller();
 }
function insertcontent() {
        for(var i = 0; i<slides.length; i++) {
               slides[i].innerHTML = contentarray[i];
     }

} 
function initscroller() {
        //console.log("scrollers")
        slides = document.getElementsByTagName("article");
        var sections = document.getElementsByTagName("section");
        var mainscroller = document.getElementById("scroller");
        
        model.sections = sections;
        model.slides = slides;
        model.internalScrollerArray = [];
                    
       for(var i = 0; i< sections.length; i++) {
       
             var sectionslides = sections[i].getElementsByTagName("article");
             model.sections[i].slides = sectionslides;
             
             if (sectionslides.length > 1) {
                   
                   var newscroller = document.createElement('div');
                   newscroller.setAttribute('class', 'verticalscroller');
                   newscroller.setAttribute('id', sections[i].id + 'scroller');
                   
                   for(var j = 0; j <sectionslides.length; j++) {
                          var clone = sectionslides[j].cloneNode(true);
                          newscroller.appendChild(clone);
                          //console.log("This section has many slides so I am making a scroller " + sections[i].id + ' slides ' + clone.id)
                   } 
                    sections[i].appendChild(newscroller);
                    clean(sections[i], sectionslides)
                    
                    vscroller = sections[i].id;
                    //console.log('Make Scroller ' + vscroller + ' section ' + sections[i].id)
                    sections[i].style.height = (708*sectionslides.length)+'px'; 
                    //console.log('Make Scroller ' + vscroller + ' section height ' + sections[i].style.height);                   
                    vscroller = new SlideScroller(sections[i].id, {
                     					          direction: 'vertical',
                     					          nrOfSlides: sectionslides.length
                     					     });
                    vscroller.id = sections[i].id; 	
                    vscroller.domId = newscroller.id;
                    model.internalScrollerArray.push(vscroller); 
             }
             
       }
       mainscroller.style.width = (1024*model.sections.length)+'px';  
       //console.log('Scroller width set: ' + mainscroller.style.width);     
       
       slideScroll =  new SlideScroller('scroller', {
 						          direction: 'horizontal',
 						          nrOfSlides: model.sections.length
 						        });
       document.addEventListener('scrollIn', scrollEnterHandler, false);	
       //document.addEventListener('scrollOut', scrollExitHandler, false);
       insertcontent();
       model.slidescroller = slideScroll;
       addHeadlineEvents();
       model.slidesready = true;
}

function clean(section, contentarray) {
     for(var i = 0; i <contentarray.length; i++) { 
            section.removeChild(contentarray[0]);
     }
     
}
function addHeadlineEvents() {
    var clickableheaders = document.getElementsByClassName('clickable');
    for (var i = 0; i<clickableheaders.length; i++) {
            clickableheaders[i].addEventListener('click', endstatetoggle, true);
    }
}
function endstatetoggle(e) {
   //console.log('hello parent article: ' + $(e.target).closest('article')[0].id);
   if ($(e.target).closest('article')[0].className.indexOf('endState') != -1) {
          //console.log('hello parent article  remove class: ' + e.target.parentNode.parentNode.localName);
          utils.removeClass($(e.target).closest('article')[0],'endState');
   } else {
         
          utils.addClass($(e.target).closest('article')[0],'endState');
   }
}
function scrollExitHandler() {
			   console.log('SLIDE OUT!! ');
}
function scrollEnterHandler () {
             slidescontroller.markActive();
}
    
});