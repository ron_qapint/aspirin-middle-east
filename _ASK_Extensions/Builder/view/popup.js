var PopUp = {

     init: function() {
     
        var refbutton = document.getElementById('references_button');
        var zoomwindows = document.getElementsByClassName('footnote');
        var currentref;
       
	    refbutton.addEventListener('click', openwindow, false);
	    
	    getcontent('referencepopups.html','referencesholder');
		getcontent('zoompopups.html','zoomholder');
		
		function getcontent(filename, parent) {
              this.filename = filename;
              this.parent = parent;
              
			  var req = new XMLHttpRequest();
			      req.open('GET', path +this.filename, false)
			      req.setRequestHeader("Content-type", "text/html");
			      req.send(null);
			      
			      if(req.readyState == 4 && req.status == 0){
			     	      content = req.responseText;
			     	      insertcontent(content, parent);  
			     } 
			     if(req.readyState == 4 && req.status == 200){
			     	      content = req.responseText;
			     	      insertcontent(content, parent);  
			     } 
		 }
		function insertcontent(content, parent) {
		       this.content = content;
		       this.parent = parent;
		        
		       var holder = document.getElementById(parent);
		       holder.innerHTML = this.content;
		 }
		function openwindow (e) {
		       showreferencescontent(e);
		       document.getElementById('popupclosebutton').addEventListener('click', closewindow, false);		      
		} 
		function closewindow () {
		       removeClass(document.getElementById('popupWindow'), 'active');
		       document.getElementById('popupclosebutton').removeEventListener('click', closewindow, false);
		 } 
		
		function showreferencescontent (e) {
		      if(model.openthumbs == false) {
		             return
		       }
		     //console.log('CURRENT ' + model.currentSlide.id)
		     var popupWindow =  document.getElementById('popupWindow');
		     var refcontent = popupWindow.getElementsByTagName("ul");
		     		  
		     for(var i = 0; i<refcontent.length; i++) {
		          refcontent[i].style.display = 'none';
		          if (refcontent[i].getAttribute('slide-id') == model.currentSlide.id) {
		                //console.log('content slide name ' + refcontent[i].getAttribute('slide-id') + ' model target  ' + model.longtouchtarget)
		              addClass(document.getElementById('popupWindow'), 'active');
		              refcontent[i].style.display = 'block';
		              document.getElementById('popupclosebutton').style.display = 'block';
		          } 
		     }
		}
		function createzooms() {
		    for(var i = 0; i< zoomwindows.length; i++) {
		          zoomwindows[i].addEventListener('click', openzoom, false);
		    } 
		}
		
		function showzoomcontent (e) {
		     var zoomWindow =  document.getElementById('zoomWindow');
		     var zoomcontent = zoomWindow.getElementsByTagName("ul");		  
		     for(var i = 0; i<zoomcontent.length; i++) {
		          zoomcontent[i].style.display = 'none';
		          if (zoomcontent[i].getAttribute('slide-id') == model.currentSlide.id) {
		                //console.log('content slide name ' + refcontent[i].getAttribute('slide-id') + ' model target  ' + model.longtouchtarget)
		              addClass(document.getElementById('zoomWindow'), 'active');
		              zoomcontent[i].style.display = 'block';
		          } 
		     }
		}
	
		function openzoom(e) {
		       showzoomcontent(e);
		       addClass(document.getElementById('zoomWindow'), 'active');
		       document.getElementById('zoomWindow').removeEventListener('click', openzoom, false);
		       document.getElementById('zoomWindow').addEventListener('click', closezoom, false);
		} 
		function closezoom() {
		       removeClass(document.getElementById('zoomWindow'), 'active');
		       document.getElementById('zoomWindow').addEventListener('click', openzoom, false);	       
		       document.getElementById('zoomWindow').removeEventListener('click', closezoom, false);
		 } 
		
	
	   function addClass(element,className) {
	   	  if (element==undefined) {return; };
	   		if (element.className.indexOf(className) <= -1) {
	   			element.className += " " + className;
	   		}
	   	}
	   	
	   function removeClass(element,className) {
		   	if (element.className.indexOf(" " + className) > -1) {
		   		element.className = element.className.replace(new RegExp(" "+className+"\\b"), "");
		   	}
		   	else if (element.className.indexOf(className) > -1) {
		   		element.className = element.className.replace(new RegExp(className+"\\b"), "");
		   	}
	   }
	   
	   createzooms();
	   
}

};
