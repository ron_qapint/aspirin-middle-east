var customPopup = {
     
     init: function() {

        var popupbuttons = document.getElementsByClassName('popupbutton');
        var popupholder =  document.getElementById('popupholder');
        var innerpopup;
        var closebutton;
         
      	for (var i = 0; i<popupbuttons.length; i++) {
	          console.log('buttons ' + popupbuttons[i].length);
	          popupbuttons[i].addEventListener('click', openwindow, false);
	    }
	   getcontent('custompopups.html','popupholder');
	   
	   function getcontent(filename, parent) {
              this.filename = filename;
              this.parent = parent;
              
			  var req = new XMLHttpRequest();
			      req.open('GET', path +this.filename, false)
			      req.setRequestHeader("Content-type", "text/html");
			      req.send(null);
			      
			      if(req.readyState == 4 && req.status == 0){
			     	      content = req.responseText;
			     	      insertcontent(content, parent);  
			     } 
		 }
		function insertcontent(content, parent) {
		       this.content = content;
		       this.parent = parent;
		       var holder = document.getElementById(parent);
		       holder.innerHTML = this.content;
		       innerpopup = document.getElementById('innerpopup');
		       closebutton = document.getElementById('customclosebutton');
		      
		 }
		function openwindow (e) {
		       showpopup(e.target);
		       popupholder.addEventListener('click', closewindow, false);		      
		} 
		function closewindow (e) {	
		       removeClass(popupholder.childNodes[0], 'active');
		       popupholder.removeEventListener('click', closewindow, false);
		 } 
		
		function showpopup (buttonelement) {
		      if(model.openthumbs == false) {
		             return
		       }
		     closebutton.style.display = 'none';  
		     var popupid = buttonelement.getAttribute('data-popup');  
		     var content = innerpopup.getElementsByTagName('div');
		     		  
		     for(var i = 0; i<content.length; i++) {
		            content[i].style.display = 'none';
		            if(content[i].id == popupid) {
		                 content[i].style.display = 'block';
		                if(content[i].className.indexOf('default') != -1) {    
		                     closebutton.style.display = 'block';
		                } 
		            }
		          } 
		         addClass(popupholder.childNodes[0], 'active');
		     }
		}
	

};
