var Navigation = (function () {


    var controller = new NavigationController();
    var topmenu;

    var init = false;

    this.__defineGetter__("init", function () { return init });
    this.__defineSetter__("init", function (val) { init = val; initnav(); });


    function markactivemenu() {
        //console.log('Mark menu ' +model.currentMenuItem)
        model.currentMenuItem = model.menuItems[model.currentSectionIndex];
        for (var i = 0; i < model.menuItems.length; i++) {
            utils.removeClass(model.menuItems[i], 'active');
        }
        utils.addClass(model.currentMenuItem, 'active');
    }


    function addEvent(navitem, slidenum) {
        navitem.element = slidenum;
        navitem.addEventListener('click', controller.gotoslide, false);
        //navitem.addEventListener('click', react,false);
        //navitem.addEventListener('click', dereact,false);
    }
    function openpage() {
        //OPEN BUILDER PAGE
        window.location.href = 'index.html';
    }

    function react(e) {

        if (e.target.tagName == 'LI') {
            utils.addClass(e.target, 'react');
        }
        if (e.target.tagName != 'LI') {
            utils.addClass(e.target.parentNode, 'react');
        }
    }
    function dereact(e) {
        if (e.target.tagName == 'LI') {
            utils.removeClass(e.target, 'react');
        }
        if (e.target.tagName != 'LI') {
            utils.removeClass(e.target.parentNode, 'react');
        }
    }
    function toggleopen() {

        if (topmenu.className != 'open') {
            utils.addClass(topmenu, 'topmenu open');
        }
    }

    function initnav() {
        //SET VAR FOR DEFAULT START AT HOME BUTTON
        var topmenuarray = model.topmenuarray;
        //console.log('Make menu one ' + topmenuarray);
        var startposition = 0;
        var topnavwrapper = document.getElementById("topnavwrapper");
        var topnav = document.createElement('nav');
        topnav.setAttribute('id', 'topnav');
        topnav.setAttribute('class', 'topnav');

        topnavwrapper.appendChild(topnav);
        var navul = document.createElement('ul');
        navul.setAttribute('id', 'nav');
        topnav.appendChild(navul);

        console.log('Make menu ' + topmenuarray);

        for (var i = 0; i < topmenuarray.length; i++) {
            //console.log('Make menu ' + topmenuarray[i])
            var navli = document.createElement('li');
            navli.innerHTML = topmenuarray[i];

            if (topmenuarray[i].indexOf('builder') == -1) {
                navli.setAttribute('class', 'menuitem');
            }
            if (topmenuarray[i].indexOf('home') != -1) {
                navli.innerHTML = '';
                navli.setAttribute('id', 'homebutton');
                navli.setAttribute('class', 'home menuitem');
            }
            if (topmenuarray[i].indexOf('builder') != -1) {
                navli.setAttribute('id', 'builder');
                navli.addEventListener('click', openpage, false);
            }
            navul.appendChild(navli);

            if (navli.id.indexOf('home') != -1) {
                startposition = navli.offsetLeft - 20;
                console.log('navigation set: Start position home button ' + startposition + navli.id);
            }

        }

        topmenu = document.getElementById("topnav");
        var items = topmenu.getElementsByClassName("menuitem");
        var navitem;
        model.menuItems = items;
        model.navScroller = topnav;

        model.startposition = -(startposition + 15);
        console.log('start menu ' + -startposition);
        model.navScroller.style.cssText = '-webkit-transform:translate3d(' + model.startposition + 'px,0px,0px);';

        var menucount = items.length;

        if (navli.id.indexOf('builder') != -1) {
            menucount = items.length - 1;
        }
        for (var i = 0; i < menucount; i++) {
            navitem = items[i];
            addEvent(navitem, i);
        }
        
        model.addEventListener('opennavigation', toggleopen, false);
        model.addEventListener('currentSectionIndex', markactivemenu, true);
        //Make it draggable
        navdrag = new DragElement(topnav.children[0], '458', '-458');
        model.navdrag = navdrag;
    }

});