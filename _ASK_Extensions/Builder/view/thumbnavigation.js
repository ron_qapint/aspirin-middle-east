var ThumbNavigation = (function(){ 
    
    var controller = new ThumbController();
    var thumbitems;
    var thumbsections;
    var sectionid;
    var thumbmenu;
    var thumbs = new Array();
    var thumbsectionsarray = new Array();
    var thumbScroll;
	var init = false;
	
	this.__defineGetter__("init",function() { return init});
	this.__defineSetter__("init",function(val) {init=val; initthumbnav();});
	
   //model.addEventListener('currentslidethumbs', markactivesection, true);
   	var prevSection;
		
	function initthumbnav() { 
	    var slidesarray = model.slidesarray;
	    var thumbmenu = document.getElementById('thumbnav');
	    //var thumbmenu = document.createElement('div'); 
	    //thumbmenu.id = 'thumbnav';

        for (var i = 0; i < slidesarray.length; i++) {
            //utils.newElement('ul',newthumbsection,thumbmenu,'section-id',slidesarray[i][0]);
	        var newthumbsection = document.createElement('ul');
	        newthumbsection.setAttribute('id', slidesarray[i][0]+'_thumbs');
	        thumbmenu.appendChild(newthumbsection);         
	        thumbsectionsarray.push(newthumbsection);
	        
	        if (slidesarray[i].length > 1) { 
	             for (var col = 0; col < slidesarray[i].length; col++) {
			             var slidethumb = document.createElement('li');
			             thumbs.push(slidethumb);
			             slidethumb.innerHTML = '<img src="content/images/thumbnails/'+slidesarray[i][col]+'.png">';
			             slidethumb.style.backgroundRepeat = "repeat-x";
			             newthumbsection.appendChild(slidethumb);
	             }
	        } else {
	            var slidethumb = document.createElement('li');
	            thumbs.push(slidethumb);
	            slidethumb.innerHTML = '<img src="content/images/thumbnails/'+slidesarray[i]+'.png">';
                slidethumb.style.backgroundRepeat = "repeat-x";
	            newthumbsection.appendChild(slidethumb);
	        } 
	        createEvents(newthumbsection);
	     }    	    
       //End for loop
	 
	   model.thumbsections = thumbsectionsarray;
	   model.addEventListener('currentslide', markactive, false);
	   model.addEventListener('openthumbs', toggleopen, false);
	   
	 }
	 function createEvents(section) {
	           section.thumbs = section.getElementsByTagName("li");
	       	   for(var i = 0; i<section.thumbs.length; i++) {
	              navitem = section.thumbs[i];
	              addEvent(navitem, section, i);     
	            }
	 
	 }
	 function markactive() {
	 
	     for (var i = 0; i< thumbs.length; i++) {
	              utils.removeClass(thumbs[i], 'current');
	        } 
	     	 
	      if (model.direction == 'horizontal') {
	              model.currentthumb = model.thumbsections[model.currentSectionIndex].thumbs[0];
	              console.log('CURRENT THUMB ' + model.currentthumb.innerHTML);
	      } else {
	            model.currentthumb = model.thumbsections[model.currentSectionIndex].thumbs[model.currentVSectionIndex];
	            console.log('CURRENT THUMB ' + model.currentthumb);
	      }
	    	 	    
	 	  utils.addClass(model.currentthumb, 'current'); 
	 }
	 	
	 function toggleopen(){
	 	   
	 	    if(!model.currentSection) { return } 
	 	     
	 	    for(var i = 0; i<thumbsectionsarray.length; i++) {
	 	         thumbsectionsarray[i].style.display = 'none';
	 	 	}
	 	    //console.log('thumbsection allowed ' + thumbsectionsarray[model.currentSectionIndex].id);
	 	    thumbsectionsarray[model.currentSectionIndex].style.display = 'block';
	 	   
	 	    if (model.openthumbs == false) {
	 	           utils.addClass(document.getElementById('thumbnav'), 'open');
	 	     }
	 		else if (model.openthumbs == true) {
	 		       utils.removeClass(document.getElementById('thumbnav'), 'open');
	 		}
	 		
	 	}
	 	 
	 function addEvent(navitem, section, slidenum) {
	 	   navitem.element = slidenum;
	 	   navitem.addEventListener('click', controller.gotoslide, false);
	 	}

});