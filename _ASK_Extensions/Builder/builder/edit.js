Object.prototype.clone = function () {
    var newObj = (this instanceof Array) ? [] : {};
    for (i in this) {
        if (i == 'clone') continue;
        if (this[i] && typeof this[i] == "object") {
            newObj[i] = this[i].clone();
        } else newObj[i] = this[i]
    } return newObj;
};

var Edit = (function () {

    var query_string = window.location.href;
    query_string = query_string.split('=');

    var query_string_value = query_string[1];
    var logLength = localStorage.length;

    var slidesarray = [];
    var listitem;
    var list;
    var numberOfSections = 0;
    var listitemarray = new Array();
    var order = 0;
    var temparray = new Array();
    var sectionsarray = new Array();
    var theLists = new Array();
    var wrapper;
    var checkvar;
    var savewindow;
    var savebutton;
    var saveallbutton;
    var playbutton;
    var addbutton;
    var allSlidesRequired;
    var allSlidesRequiredOKButton;
    var sectionCannotBeDeselected;
    var sectionCannotBeDeselectedOKButton;
    var slideCannotBeDeselected;
    var slideCannotBeDeselectedOKButton;
    var tempslidesarray = new Array();
    var savedslidesarray = new Array();
    var menuarray = new Array();
    var currentlink;
    var slidesadded = false;
    var presname;
    var slideswrapper;
    var slidesaddedarray = new Array();
    var originalOrder;
    var selectionsarrayCopy;

    this.init = function () {
        $('#editPresentationText').html(window.localisation.getString('editPage', 'editPresentationText'));
        $('#saveDialogText').html(window.localisation.getString('editPage', 'saveDialogText'));
        $('#saveallbutton').html(window.localisation.getString('editPage', 'saveallbutton'));
        $('#cancelsave').html(window.localisation.getString('editPage', 'cancelsave'));
        $('#playPresentationDialogTitle').html(window.localisation.getString('editPage', 'playPresentationDialogTitle'));
        $('#playYes').html(window.localisation.getString('editPage', 'playYes'));
        $('#playNo').html(window.localisation.getString('editPage', 'playNo'));
        $('#allSlidesRequiredDialogTitle').html(window.localisation.getString('editPage', 'allSlidesRequiredDialogTitle'));
        $('#allSlidesRequiredDialogText').html(window.localisation.getString('editPage', 'allSlidesRequiredDialogText'));
        $('#allSlidesRequiredOKButton').html(window.localisation.getString('editPage', 'allSlidesRequiredOKButton'));
        $('#sectionCannotBeDeselectedTitle').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedTitle'));
        $('#sectionCannotBeDeselectedText').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedText'));
        $('#sectionCannotBeDeselectedOKButton').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedOKButton'));
        $('#slideCannotBeDeselectedTitle').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedTitle'));
        $('#slideCannotBeDeselectedText').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedText'));
        $('#slideCannotBeDeselectedOKButton').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedOKButton'));
        $('#okButton').html(window.localisation.getString('editPage', 'okButton'));

        $('#editPresentationText').html(window.localisation.getString('editPage', 'editPresentationText'));
        $('#saveDialogText').html(window.localisation.getString('editPage', 'saveDialogText'));
        $('#saveallbutton').html(window.localisation.getString('editPage', 'saveallbutton'));
        $('#cancelsave').html(window.localisation.getString('editPage', 'cancelsave'));
        $('#playPresentationDialogTitle').html(window.localisation.getString('editPage', 'playPresentationDialogTitle'));
        $('#playYes').html(window.localisation.getString('editPage', 'playYes'));
        $('#playNo').html(window.localisation.getString('editPage', 'playNo'));
        $('#allSlidesRequiredDialogTitle').html(window.localisation.getString('editPage', 'allSlidesRequiredDialogTitle'));
        $('#allSlidesRequiredDialogText').html(window.localisation.getString('editPage', 'allSlidesRequiredDialogText'));
        $('#allSlidesRequiredOKButton').html(window.localisation.getString('editPage', 'allSlidesRequiredOKButton'));
        $('#sectionCannotBeDeselectedTitle').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedTitle'));
        $('#sectionCannotBeDeselectedText').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedText'));
        $('#sectionCannotBeDeselectedOKButton').html(window.localisation.getString('editPage', 'sectionCannotBeDeselectedOKButton'));
        $('#slideCannotBeDeselectedTitle').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedTitle'));
        $('#slideCannotBeDeselectedText').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedText'));
        $('#slideCannotBeDeselectedOKButton').html(window.localisation.getString('editPage', 'slideCannotBeDeselectedOKButton'));
        $('#okButton').html(window.localisation.getString('editPage', 'okButton'));

        selectionsarrayCopy = selectionsarray.clone();
        slideswrapper = document.getElementById("slidethumbs1");
        presname = query_string_value;
        var pagetitle = document.getElementById("pagetitle");
        var actualName = presname.split('&')[0];
        pagetitle.innerHTML = window.localisation.getString('editPage', 'pagetitle') + ' ' + actualName;

        savebutton = document.getElementById("save");
        addbutton = document.getElementById("addslides");
        playbutton = document.getElementById("play");
        previousButton = document.getElementById("previous");
        utils.removeClass(previousButton, 'disabled');
        //        previousButton.addEventListener('click', openpage, false);
        var returnbutton = document.getElementById("return");
        returnbutton.href = './showplaylist.html';
        returnbutton.addEventListener('click', openMenu, false);

        previousButton.addEventListener('click', openbuilder, false);

        //returnbutton.addEventListener('click', openpage, false);
        //POPUP SAVE
        saveallbutton = document.getElementById("saveallbutton");
        var cancelbutton = document.getElementById("cancelsave");

        savewindow = document.getElementById("savewindow");
        playPresentationWindow = document.getElementById("playPresentation");
        addbutton.addEventListener('click', savechapters, false);
        //savebutton.addEventListener('click', openpopupsave, false);
        utils.addClass(savebutton, 'disabled');

        cancelbutton.addEventListener('click', cancelsave, false);
        saveallbutton.addEventListener('click', savepresentation, false);
        playbutton.addEventListener('click', openlink, false);
        utils.addClass(playbutton, 'disabled');

        playPresentationButton = document.getElementById("playYes");
        notPlayPresentationButton = document.getElementById("playNo");

        allSlidesRequired = document.getElementById("allSlidesRequired");
        allSlidesRequiredOKButton = document.getElementById("allSlidesRequiredOKButton");

        sectionCannotBeDeselected = document.getElementById("sectionCannotBeDeselected");
        sectionCannotBeDeselectedOKButton = document.getElementById("sectionCannotBeDeselectedOKButton");

        slideCannotBeDeselected = document.getElementById("slideCannotBeDeselected");
        slideCannotBeDeselectedOKButton = document.getElementById("slideCannotBeDeselectedOKButton");

        playPresentationButton.addEventListener('click', openlink, false);
        notPlayPresentationButton.addEventListener('click', openMenu, false);

        allSlidesRequiredOKButton.addEventListener('click', closeAllSlidesRequiredWindow, false);
        sectionCannotBeDeselectedOKButton.addEventListener('click', closeSectionCannotBeDeselectedWindow, false);
        slideCannotBeDeselectedOKButton.addEventListener('click', closeSlideCannotBeDeselected, false);

        document.getElementById('errorwindow').addEventListener('click', closeerror, false);

        wrapper = document.getElementById("chapters");
        utils.addClass(wrapper, 'unsorted');
        list = document.createElement('ul');

        list.setAttribute('id', 'chapterlist');
        list.setAttribute('class', 'chapterlist');

        wrapper.appendChild(list);

        originalOrder = chapterarray.clone();

        for (var i = 0; i < chapterarray.length; i++) {
            if (chapterarray[i].substring(0, 1) != "-") {
                listitem = document.createElement('li');

                var ca = chapterarray[i];

                if (ca.substring(0, 1) == "+" || ca.substring(0, 1) == "!") {
                    ca = ca.substring(1);
                }

                listitem.innerHTML = ca + '<span></span>';
                listitem.setAttribute('id', chapterarray[i]);
                listitem.code = chapterarray[i];
                listitem.sectioncode = selectionsarray[i][0];
                listitem.slides = selectionsarray[i];

                list.appendChild(listitem);
                listitem.addEventListener('click', sortarray, false);
                listitemarray.push(listitem);

            }
        }
        listitem = document.createElement('li');
        listitem.style.visibility = 'hidden';
        listitem.innerHTML = '<span>&nbsp</span>';
        list.appendChild(listitem);
        edit();
    }

    function closeAllSlidesRequiredWindow() {
        utils.addClass(allSlidesRequired, 'hide');
        utils.removeClass(allSlidesRequired, 'show');
    }
    function closeSectionCannotBeDeselectedWindow() {
        utils.addClass(sectionCannotBeDeselected, 'hide');
        utils.removeClass(sectionCannotBeDeselected, 'show');
    }
    function closeSlideCannotBeDeselected() {
        utils.addClass(slideCannotBeDeselected, 'hide');
        utils.removeClass(slideCannotBeDeselected, 'show');
    }

    function edit() {

        var savetitle = document.getElementById("savetitle");
        var indexOfAnd = presname.indexOf('&');
        savetitle.innerHTML = presname.substring(0, indexOfAnd);

        var savedvalues = localStorage.getItem(query_string_value);
        var keyid = query_string_value.split('&');
        console.log('key ' + keyid[1]);

        if (keyid[1] == model.appkey.split('&')[1]) {
            //console.log('JSON ' + JSON.parse(savedvalues));
            var editarray = JSON.parse(savedvalues);

            if (localStorage.getItem(query_string_value)) {
                //values = editarray.split(","); 
                for (var i = editarray.length - 1; i >= 0; i--) {
                    numberOfSections++;
                    var sectionvalue = editarray[i][0];
                    //console.log('sections ' +  temparray);
                    markselectedsection(sectionvalue);
                    var slidevalue = editarray[i][0];
                    savedslidesarray.push(slidevalue);
                    if (editarray[i].length > 1) {
                        for (var j = 0; j < editarray[i].length; j++) {
                            var slidevalue = editarray[i][j];
                            var found = false;
                            for (var t = 0; t < savedslidesarray.length; t++) {
                                if (savedslidesarray[t] == slidevalue) {
                                    found = true;
                                }
                            }
                            if (!found) {
                                savedslidesarray.push(slidevalue);
                            }
                        }
                    }
                }
                var reversedSaved = savedslidesarray.reverse();
                var reversedSavedNoDuplicates = new Array();

                for (var i = 0; i < reversedSaved.length; i++) {
                    var found = false;
                    for (var j = 0; j < reversedSavedNoDuplicates.length; j++) {
                        if (reversedSaved[i] == reversedSavedNoDuplicates[j]) {
                            found = true;
                        }
                    }
                    if (!found) {
                        reversedSavedNoDuplicates.push(reversedSaved[i]);
                    }
                }
                reversedSaved = reversedSavedNoDuplicates;

                for (var i = 0; i < reversedSaved.length; i++) {
                    for (var j = 0; j < selectionsarray.length; j++) {
                        for (var k = 0; k < selectionsarray[j].length; k++) {
                            if (selectionsarray[j][k] == reversedSaved[i]) {
                                selectionsarray[j].splice(k, 1);
                                selectionsarray[j].unshift(reversedSaved[i]);
                            }
                        }
                    }
                }
            }
        }
    }
    function markselectedsection(listelement) {
        //order = 0;
        for (var i = 0; i < listitemarray.length; i++) {
            var chosenitem = listitemarray[i];
            if (listitemarray[i].slides.length == 1) {
                var section = listitemarray[i].sectioncode;
                if (listelement == section) {
                    var ca = listitemarray[i].code;
                    var forced = false;
                    if (ca.substring(0, 1) == "!") {
                        forced = true;
                    }
                    temparray.push(listitemarray[i].sectioncode);
                    utils.addClass(listitemarray[i], 'active');
                    var chosenitem = listitemarray[i];
                    console.log('list ' + chosenitem);
                    chosenitem.removeEventListener('click', sortarray, false);
                    chosenitem.addEventListener('click', sortreset, false);
                    if (forced) {
                        chosenitem.removeEventListener('click', sortreset, false);
                        chosenitem.addEventListener('click', showmessage, false);
                    }
                    order++;
                    list.removeChild(chosenitem);
                    list.insertBefore(chosenitem, list.childNodes[0]);
                }

            }
            if (listitemarray[i].slides.length > 1) {
                for (var j = 0; j < listitemarray[i].slides.length; j++) {
                    var section = listitemarray[i].slides[j];
                    if (listelement == section) {
                        var ca = listitemarray[i].code;
                        var forced = false;
                        if (ca.substring(0, 1) == "!") {
                            forced = true;
                        }
                        temparray.push(listitemarray[i].sectioncode);
                        utils.addClass(listitemarray[i], 'active');
                        var chosenitem = listitemarray[i];
                        chosenitem.removeEventListener('click', sortarray, false);
                        chosenitem.addEventListener('click', sortreset, false);
                        if (forced) {
                            chosenitem.removeEventListener('click', sortreset, false);
                            chosenitem.addEventListener('click', showmessage, false);
                        }

                        order++;
                        list.removeChild(chosenitem);
                        list.insertBefore(chosenitem, list.childNodes[0]);
                    }
                }
            }
        }
        var index = chapterarray.indexOf(chosenitem);

        chapterarray.splice(index, 1);
        chapterarray.unshift(chosenitem.code);


    }
    function openpage(e) {
        //addClass(element, 'active');
        window.location.href = window.location.href;
    }
    function openlink() {
        if (currentlink == undefined || currentlink == null) {
            return
        }
        var linkId = currentlink;
        window.location.href = '../../index.html?=' + linkId;
    }

    function openbuilder() {
        window.location.href = './showplaylist.html';
    }

    function openMenu() {
        window.location.href = './index.html';
    }

    function savechapters() {
        console.log('Temp array ' + temparray);

        if (temparray.length == 0) {
            errorpopup();
            errorstr = window.localisation.getString('pagetitle', 'saveChapterFirst')
            document.getElementById('errordialog').innerHTML = '<h1>' + errorstr + '</h1>';
            return
        }

        for (var i = 0; i < temparray.length; i++) {
            sectionsarray.push([temparray[i]]);
            console.log('Sections saved array ' + sectionsarray);
        }

        //BUG
        //alert('tets');
        previousButton.removeEventListener();
        previousButton.addEventListener('click', openpage, false);


        orderchapters();

        //        document.getElementById('agendatitle').innerHTML = '<h1></h1>';
        utils.removeClass(savebutton, 'disabled');
        utils.addClass(addbutton, 'disabled');

        addbutton.removeEventListener('click', savechapters, false);
        savebutton.addEventListener('click', opensave, false);
        //        document.getElementById('pagetitle').innerHTML = window.localisation.getString('editPage', 'pagetitle2');
        document.getElementById('editPresentationText').innerHTML = window.localisation.getString('editPage', 'editPresentationText');
        //document.getElementById('agendatitle').innerHTML = '<h1>Edit Presentation</h1><p>Please select the slides you would like to include in your presentation and then click "NEXT".</p>';
        utils.removeClass(previousButton, 'disabled');
    }
    function errorpopup(e, errorstr) {
        utils.addClass(errorwindow, 'show');

    }
    function opensave() {
        console.log('slidesaddedarray.length ' + slidesaddedarray.length);
        console.log('Slides array ' + tempslidesarray[0].slides);
        if (slidesaddedarray.length == 0) {
            errorpopup();
            errorstr = window.localisation.getString('editPage', 'selectSlidesFirst');
            document.getElementById('errordialog').innerHTML = '<h1>' + errorstr + '</h1>';
            return
        }
        console.log('Sections saved array ' + slidesarray);

        var ok = true;
        for (var p = 0; p < tempslidesarray.length; p++) {
            var section = tempslidesarray[p];
            if (section.section.numberOfSlidesSelected < 1) {
                ok = false;
            }
        }

        if (!ok) {
            errorpopup();
            errorstr = window.localisation.getString('editPage', 'selectSlidesFirst');
            document.getElementById('errorMessage').innerHTML = errorstr;
            console.log('OUT ' + section);
            return
        }

        for (var i = 0; i < tempslidesarray.length; i++) {
            if (tempslidesarray[i].clickstate === 0) {
                var sectionindex = slidesarray.indexOf(tempslidesarray[i].slides);
                var section = slidesarray[sectionindex];
                var slideindex = section.indexOf(tempslidesarray[i].code);
                section.splice(slideindex, 1);
                console.log('OUT ' + tempslidesarray[i].clickstate);
            }
        }
        utils.addClass(savewindow, 'show');

    }
    function SlidesNumber() {

    }
    function savepresentation() {
        var presId = query_string_value;

        for (var i = 0; i < slidetoolsarray.length; i++) {
            slidesarray.unshift(slidetoolsarray[i]);
        }
        for (var p = 0; p < tempslidesarray.length; p++) {
            if (tempslidesarray[p].clickstate == 0) {
                var sectionindex = slidesarray.indexOf(tempslidesarray[p].slides);
                var section = slidesarray[sectionindex];
                var slideindex = section.indexOf(tempslidesarray[p].code);
                //section.splice(slideindex, 1);
                if (section.length == 0) {
                    slidesarray.splice(sectionindex, 1);
                    console.log('OUT ' + slidesarray);
                }
            }
        }
        if (slidesarray.length == 0) {
            return
        }
        //console.log('Slides array ' + slidesarray + ' section ' + section);
        //console.log('Pres name: ' + presId);
        //console.log('JSarray ' + jsarray);
        utils.removeClass(savewindow, 'show');
        //utils.removeClass(playbutton, 'disabled');
        utils.addClass(savebutton, 'disabled');
        $("#footer img#select").hide();
        $("#footer img#playBreadCrumb").show();
        savebutton.removeEventListener('click', opensave, false);

        //document.getElementById('agendatitle').innerHTML = '<h1>Edit Presentation</h1><p>Please click "PLAY".</p>';

        commitvalues(presId, JSON.stringify(slidesarray));
        utils.removeClass(playPresentationWindow, 'hide');
        utils.addClass(playPresentationWindow, 'show');
    }
    function commitvalues(presId, slidesarray, jsarray) {

        var key = presId;
        //CREATE LINKS ITEM
        if (localStorage.getItem('links')) {
            //Links holder
            var linkarray = new Array();
            var oldarrayvalues = localStorage.getItem('links');
            values = oldarrayvalues.split(","); //create an array of the values
            //Add existing values to link array
            for (var i = 0; i < values.length; i++) {
                linkarray.push(values[i]);
                if (presId == values[i]) {
                    //console.log('saving the same!!' + linkarray);
                    linkarray.splice(linkarray.indexOf(values[i]), 1);
                    //console.log('OUT!!' + linkarray);
                }
            }
            //Add new value to link array
            linkarray.push(presId);
            //Save array with new values
            localStorage.setItem('links', linkarray);
        } else {
            //Create local storage item if none
            localStorage.setItem('links', presId);
        }
        //CREATE PRESENTATION ITEM
        localStorage.setItem(key, slidesarray);
        localStorage.setItem(key + 'menu', menuarray);
        currentlink = key;
    }

    function cancelsave() {
        utils.removeClass(savewindow, 'show');
    }
    function closeerror() {
        utils.removeClass(errorwindow, 'show');
        document.getElementById('errorwindow').childNodes[0].innerHTML = '';
    }
    function orderchapters() {
        $("#return").html('');
        //$("#return").attr('onclick', 'javascript: window.location=window.location; return false;');
        wrapper.removeChild(list);
        var slideswrapper1 = document.getElementById("slidethumbs1");
        var slideswrapper2 = document.getElementById("slidethumbs2");

        if (allowOrderingOfSections) {
            for (var j = sectionsarray.length - 1; j >= 0; j--) {
                for (var i = 0; i < listitemarray.length; i++) {
                    var item = listitemarray[i];
                    var fixed = false;
                    if (item.code.substring(0, 1) == "+" || item.code.substring(0, 1) == "!") {
                        fixed = true;
                    }
                    if (item.sectioncode == sectionsarray[j]) {
                        var newlist = document.createElement('ul');
                        item.setAttribute('class', 'chaptertitle');
                        menuarray.push(item.innerHTML);
                        slideswrapper1.appendChild(newlist);
                        console.log('menu ' + menuarray);
                        newlist.appendChild(item);
                        addslides(sectionsarray[j], newlist, fixed);
                    }
                }
            }
        }
        else {
            for (var i = 0; i < listitemarray.length; i++) {
                var item = listitemarray[i];
                //var item = listitemarray[i];
                var fixed = false;
                if (item.code.substring(0, 1) == "+") {
                    fixed = true;
                }
                //console.log('Compare ' + item.sectioncode);
                for (var j = 0; j < sectionsarray.length; j++) {
                    if (item.sectioncode == sectionsarray[j]) {

                        var newlist = document.createElement('ul');
                        item.setAttribute('class', 'chaptertitle');
                        menuarray.push(item.innerHTML);
                        slideswrapper1.appendChild(newlist);
                        console.log('menu ' + menuarray);
                        newlist.appendChild(item);
                        addslides(sectionsarray[j], newlist, fixed);
                    }
                }
            }
        }
        utils.removeClass(wrapper, 'unsorted');
        utils.addClass(wrapper, 'sorted');
        utils.addClass(slideswrapper1, 'sorted');
    }
    function addslides(section, sectionlist, fixed) {
        this.sectionlist = sectionlist;
        checkvar = 'Checkbox';
        var b = $("#slidethumbs1").height();
        if (b < 400) {
            $("#downArrow").hide();
        } else {
            $("#downArrow").show();
        }
        if (numberOfSections > 4) {
            slideswrapper.style.width = '3072px';
            document.addEventListener('swipeleft', scrollleft);
            document.addEventListener('swiperight', scrollright);
            document.getElementById("arrowRight").style.display = "block";
            document.getElementById("arrowLeft").style.display = "none";
        }
        else {
            document.getElementById("arrowRight").style.display = "none";
            document.getElementById("arrowLeft").style.display = "none";
        }

        for (var i = 0; i < selectionsarray.length; i++) {
            var found = false;
            for (var t = 0; t < selectionsarray[i].length; t++) {
                if (section == selectionsarray[i][t]) {
                    found = true;
                }
            }
            var orderCounter = 1;
            if (found) {
                var sectionslidesarray = selectionsarray[i];
                //console.log('Sections - get slides: ' + selectionsarray[i]);
                slidesarray.push(selectionsarray[i]);
                section.numberOfSlidesSelected = 0;
                section.theOrder = 1;
                if (fixed) {
                    for (var j = sectionslidesarray.length - 1; j >= 0; j--) {
                        slidelistitem = document.createElement('li');
                        var forced = false;
                        var ca = sectionslidesarray[j];
                        if (ca.substring(0, 1) == "+") {
                            forced = true;
                            ca = ca.substring(1);
                        }
                        //slidelistitem.innerHTML = '<img src="content/images/thumbnails/'+sectionslidesarray[j]+'.png">';

                        /* added AD - 30/03/2012 */
                        slidelistitem.innerHTML = '<span></span>';
                        slidelistitem.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + ca + '.jpg)  no-repeat 3px  5px;';
                        slidelistitem.slidethumb = ca;
                        slidelistitem.setAttribute('id', sectionslidesarray[j] + '_slide');
                        slidelistitem.setAttribute('class', 'slideitem');

                        slidelistitem.code = ca;
                        slidelistitem.slides = sectionslidesarray;

                        slidelistitem.section = section;
                        slidelistitem.clickstate = 0;
                        slidelistitem.slidesorder = 0;

                        slidelistitem.addEventListener('click', sortslidesarray, false);
                        slidelistitem.sectionlist = this.sectionlist;
                        sectionlist.appendChild(slidelistitem);
                        tempslidesarray.push(slidelistitem);
                        markselectedslide(slidelistitem, true);

                        if (forced) {
                            slidelistitem.removeEventListener('click', sortslidesreset, false);
                            slidelistitem.addEventListener('click', showslidemessage, false);
                        }
                    }
                }
                else {
                    for (var j = sectionslidesarray.length - 1; j >= 0; j--) {
                        slidelistitem = document.createElement('li');
                        //slidelistitem.innerHTML = '<img src="content/images/thumbnails/'+sectionslidesarray[j]+'.png">';

                        /* added AD - 30/03/2012 */
                        slidelistitem.innerHTML = '<span></span>';
                        slidelistitem.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + sectionslidesarray[j] + '.jpg)  no-repeat 3px  5px;';
                        slidelistitem.slidethumb = sectionslidesarray[j];
                        slidelistitem.setAttribute('id', sectionslidesarray[j] + '_slide');
                        slidelistitem.setAttribute('class', 'slideitem');
                        slidelistitem.code = sectionslidesarray[j];
                        slidelistitem.slides = sectionslidesarray;

                        slidelistitem.section = section;
                        slidelistitem.clickstate = 0;
                        slidelistitem.slidesorder = 0;


                        var actualOrder;
                        for (y = 0; y < selectionsarrayCopy.length; y++) {
                            for (k = 0; k < selectionsarrayCopy[y].length; k++) {
                                if (selectionsarrayCopy[y][k] == sectionslidesarray[j]) {
                                    actualOrder = k;
                                }
                            }
                        }
                        slidelistitem.originalOrder = actualOrder;

                        slidelistitem.addEventListener('click', sortslidesarray, false);
                        slidelistitem.sectionlist = this.sectionlist;
                        sectionlist.appendChild(slidelistitem);
                        tempslidesarray.push(slidelistitem);
                        markselectedslide(slidelistitem, false);

                        if (fixed) {
                            slidelistitem.removeEventListener('click', sortslidesreset, false);
                        }
                    }
                }
                theLists.push(sectionlist);
                //slidelistitem.slides.reverse();
            }
        }

    }
    function markselectedslide(listelement, fixed) {
        //console.log('element ' + listelement.code);
        for (var i = 0; i < savedslidesarray.length; i++) {

            //console.log('SAVED ' + savedslidesarray[i] + ' element ' + listelement.code);
            if (listelement.code === savedslidesarray[i] || fixed) {
                //console.log('match ' + listelement);
                listelement.clickstate = 1;
                listelement.style.cssText = 'background: url(images/agenda/Checkbox_flagged.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + listelement.slidethumb + '.jpg)  no-repeat 3px  5px;';
                listelement.setAttribute('class', 'activeThumb');
                var index = listelement.slides.indexOf(listelement.code);
                var mainindex = slidesarray.indexOf(listelement.slides);

                var listNode = listelement.sectionlist;
                listelement.sectionlist.removeChild(listelement);
                listelement.sectionlist.appendChild(listelement);
                listelement.sectionlist.insertBefore(listelement, listNode.childNodes[1]);
                listelement.slidesorder++;
                listelement.section.theOrder++;
                listelement.section.numberOfSlidesSelected++;
                slidesaddedarray.push(listelement.code);

                listelement.removeEventListener('click', sortslidesarray, false);
                listelement.addEventListener('click', sortslidesreset, false);
            }
            else {
                var index = listelement.slides.indexOf(listelement.code);
                var mainindex = slidesarray.indexOf(listelement.slides);
                var listNode = listelement.sectionlist;
                listelement.sectionlist.removeChild(listelement);
                listelement.sectionlist.appendChild(listelement);
                listelement.sectionlist.insertBefore(listelement, listNode.childNodes[1]);
            }
        }
    }
    function sortslidesarray(e) {

        slidesaddedarray.push(e.target.code);
        slidesadded = true;
        e.target.clickstate = 1;
        checkvar = 'Checkbox_flagged';
        e.target.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + e.target.slidethumb + '.jpg)  no-repeat 3px  5px;';

        e.target.setAttribute('class', 'activeThumb');


        if (allowOrderingOfSlides) {
            var index = e.target.slides.indexOf(e.target.code);
            var mainindex = slidesarray.indexOf(e.target.slides); ;
            var listNode = e.target.sectionlist;
            console.log('Sort me: ' + e.target.code + ' in ' + slidesarray + ' at ' + e.target.slidesorder);
            e.target.slides.splice(index, 1);
            e.target.slides.splice(e.target.section.theOrder, 0, e.target.code);
            e.target.sectionlist.removeChild(e.target);
            e.target.sectionlist.insertBefore(e.target, listNode.childNodes[e.target.section.theOrder]);
            e.target.section.theOrder++;
            e.target.slidesorder++;
        }
        e.target.section.numberOfSlidesSelected++;
        e.target.removeEventListener('click', sortslidesarray, false);
        e.target.addEventListener('click', sortslidesreset, false);

    }

    function sortslidesreset(e) {
        var numberOfSlidesSelected = e.target.parentNode.section.theOrder - 1;
        slidesaddedarray.pop(e.target.code);
        checkvar = 'Checkbox';
        e.target.style.cssText = ' url(../../content/img/thumbs/' + e.target.slidethumb + '.jpg)  no-repeat 0px  0px;';
        e.target.parentNode.clickstate = 0;
        e.target.parentNode.setAttribute('class', 'slideitem');
        var index = e.target.parentNode.slides.indexOf(e.target.code);
        var listNode = e.target.parentNode.sectionlist;
        console.log('slides array remove' + listNode.code + ' at index ' + e.target.parentNode.slidesorder)
        if (allowOrderingOfSlides) {
            listNode.removeChild(e.target.parentNode);
            var lis = listNode.childNodes;
            var found = false;
            if (e.target.parentNode.originalOrder == 1) {
                listNode.insertBefore(e.target.parentNode, lis[numberOfSlidesSelected]);
                found = true;
            }
            else {
                for (i = numberOfSlidesSelected - 1; i < lis.length; i++) {
                    if (e.target.parentNode.originalOrder > lis[i].originalOrder) {
                        if ((i + 1) < lis.length) {
                            listNode.insertBefore(e.target.parentNode, lis[i + 1]);
                        }
                        else {
                            listNode.appendChild(e.target.parentNode);
                        }
                        found = true;
                    }
                }
            }
            if (!found) {
                listNode.insertBefore(e.target.parentNode, lis[numberOfSlidesSelected]);
            }
            e.target.parentNode.section.theOrder--;
            e.target.parentNode.slidesorder--;
        }
        e.target.parentNode.section.numberOfSlidesSelected--;
        e.target.parentNode.addEventListener('click', sortslidesarray, false);
        e.target.parentNode.removeEventListener('click', sortslidesreset, false);
    }


    function sortarray(e) {

        var index = chapterarray.indexOf(e.target.code);
        utils.addClass(e.target, 'active');
        if (allowOrderingOfSections) {
            temparray.unshift(e.target.sectioncode);
        }
        else {
            temparray.push(e.target.sectioncode);
        }

        if (e.target.code.substring(0, 1) == "+") {
            utils.addClass(allSlidesRequired, 'show');
            utils.removeClass(allSlidesRequired, 'hide');
        }

        if (allowOrderingOfSections) {
            chapterarray.splice(index, 1);
            chapterarray.unshift(e.target.code);

            list.removeChild(e.target);
            list.insertBefore(e.target, list.childNodes[order]);

            order++;
        }
        numberOfSections++;
        e.target.removeEventListener('click', sortarray, false);
        e.target.addEventListener('click', sortreset, false);
    }
    function sortreset(e) {

        var index = temparray.indexOf(e.target.sectioncode);
        temparray[index] = "-none";
        nextFreeSpace = index;
        console.log('Sections array remove' + e.target.code + ' at index ' + index)

        utils.removeClass(e.target, 'active');
        e.target.addEventListener('click', sortarray, false);
        e.target.removeEventListener('click', sortreset, false);
        list.removeChild(e.target);
        var currentArray = new Array();
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == false) {
                currentArray.push(list.childNodes[index].code);
            }
        });

        var newPosition = getCurrentPosition(originalOrder, currentArray, e.target.code);
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == true && index <= newPosition) {
                newPosition++;
            }
        });
        var oneAhead = list.childNodes[newPosition];
        list.insertBefore(e.target, oneAhead);

        nextFreeSpace = 0;
        order = 0;
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == true) {
                nextFreeSpace++;
                order++;
            }
        });
        numberOfSections--;
    }

    function getCurrentPosition(original, current, code) {
        var originalPosition;
        for (i = 0; i < original.length; i++) {
            if (original[i] == code) {
                originalPosition = i;
            }
        }
        if (originalPosition == 0) {
            return 0;
        }
        var codesBefore = new Array();
        for (i = 0; i < originalPosition; i++) {
            codesBefore.push(original[i]);
        }
        var newPosition = 0;
        var shouldBreak = false;
        for (i = codesBefore.length - 1; i > -1; i--) {
            if (shouldBreak) {
                break;
            }
            for (j = current.length - 1; j > -1; j--) {
                //alert("a: " + codesBefore[i] + ", b: " + current[j]);
                if (current[j] == codesBefore[i]) {
                    newPosition = j + 1;
                    shouldBreak = true;
                    break;
                }
            }
        }
        return newPosition;
    }

    function showmessage() {
        utils.addClass(sectionCannotBeDeselected, 'show');
        utils.removeClass(sectionCannotBeDeselected, 'hide');
    }

    function showslidemessage() {
        utils.addClass(slideCannotBeDeselected, 'show');
        utils.removeClass(slideCannotBeDeselected, 'hide');
    }

    var page = 1;


    function scrollleft() {

        if (page == 1 && numberOfSections > 4) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-1024px,0px,0px);';
            page++;
            if (numberOfSections > 8) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "block";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
        else if (page == 2 && numberOfSections > 8) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-2048px,0px,0px);';
            page++;
            if (numberOfSections > 12) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "none";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
    }
    function scrollright() {
        if (page == 2) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(0px,0px,0px);';
            page--;
            if (numberOfSections > 4) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "none";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
        else if (page == 3) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-1024px,0px,0px);';
            page--;
            if (numberOfSections > 8) {
                document.getElementById("arrowRight").style.display = "block";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
            }
        }
    }
});	 
