Object.prototype.clone = function () {
    var newObj = (this instanceof Array) ? [] : {};
    for (i in this) {
        if (i == 'clone') continue;
        if (this[i] && typeof this[i] == "object") {
            newObj[i] = this[i].clone();
        } else newObj[i] = this[i]
    } return newObj;
};

var Builder = (function () {

    var listitem;
    var list;
    var listitemarray = new Array();
    var order = 0;
    var numberOfSections;
    var temparray = new Array();
    var sectionsarray = new Array();
    var selectedArray = new Array();
    var wrapper;
    var checkvar;
    var savewindow;
    var nameAlreadyExistsWindow;
    var nameAlreadyExistsOkButton;
    var pleaseEnterNameWindow;
    var pleaseEnterNameWindowOKButton;
    var allSlidesRequired;
    var allSlidesRequiredOKButton;
    var sectionCannotBeDeselected;
    var sectionCannotBeDeselectedOKButton;
    var slideCannotBeDeselected;
    var slideCannotBeDeselectedOKButton;

    var savebutton;
    var saveallbutton;
    var playbutton;
    var addbutton;
    var tempslidesarray = new Array();
    var menuarray = new Array();
    var currentlink;
    var slidesadded = false;
    var slidesaddedarray = new Array();
    var slideswrapper;
    var slidesarray = [];
    var nextFreeSpace;
    var slidesNumber;
    var originalOrder;

    this.init = function () {
        $('#createNewPresentationTitle').html(window.localisation.getString('builderPage', 'createNewPresentationTitle'));
        $('#createNewPresentationText').html(window.localisation.getString('builderPage', 'createNewPresentationText'));
        $('#saveDialogTitle').html(window.localisation.getString('builderPage', 'saveDialogTitle'));
        $('#saveDialogText').html(window.localisation.getString('builderPage', 'saveDialogText'));
        $('#saveallbutton').html(window.localisation.getString('builderPage', 'saveallbutton'));
        $('#cancelsave').html(window.localisation.getString('builderPage', 'cancelsave'));
        $('#nameAlreadyExistsDialogTitle').html(window.localisation.getString('builderPage', 'nameAlreadyExistsDialogTitle'));
        $('#nameAlreadyExistsDialogText').html(window.localisation.getString('builderPage', 'nameAlreadyExistsDialogText'));
        $('#nameAlreadyExistsOKButton').html(window.localisation.getString('builderPage', 'nameAlreadyExistsOKButton'));
        $('#pleaseEnterNameDialogTitle').html(window.localisation.getString('builderPage', 'pleaseEnterNameDialogTitle'));
        $('#pleaseEnterNameDialogText').html(window.localisation.getString('builderPage', 'pleaseEnterNameDialogText'));
        $('#pleaseEnterNameWindowOKButton').html(window.localisation.getString('builderPage', 'pleaseEnterNameWindowOKButton'));
        $('#allSlidesRequiredDialogTitle').html(window.localisation.getString('builderPage', 'allSlidesRequiredDialogTitle'));
        $('#allSlidesRequiredDialogText').html(window.localisation.getString('builderPage', 'allSlidesRequiredDialogText'));
        $('#allSlidesRequiredOKButton').html(window.localisation.getString('builderPage', 'allSlidesRequiredOKButton'));
        $('#sectionCannotBeDeselectedTitle').html(window.localisation.getString('builderPage', 'sectionCannotBeDeselectedTitle'));
        $('#sectionCannotBeDeselectedText').html(window.localisation.getString('builderPage', 'sectionCannotBeDeselectedText'));
        $('#sectionCannotBeDeselectedOKButton').html(window.localisation.getString('builderPage', 'sectionCannotBeDeselectedOKButton'));
        $('#slideCannotBeDeselectedTitle').html(window.localisation.getString('builderPage', 'slideCannotBeDeselectedTitle'));
        $('#slideCannotBeDeselectedText').html(window.localisation.getString('builderPage', 'slideCannotBeDeselectedText'));
        $('#slideCannotBeDeselectedOKButton').html(window.localisation.getString('builderPage', 'slideCannotBeDeselectedOKButton'));
        $('#playPresentationDialogTitle').html(window.localisation.getString('builderPage', 'playPresentationDialogTitle'));
        $('#playPresentationDialogText').html(window.localisation.getString('builderPage', 'playPresentationDialogText'));
        $('#playYes').html(window.localisation.getString('builderPage', 'playYes'));
        $('#playNo').html(window.localisation.getString('builderPage', 'playNo'));
        $('#okButton').html(window.localisation.getString('builderPage', 'okButton'));
        okButton

        slideswrapper = document.getElementById("slidethumbs1");

        savebutton = document.getElementById("save");
        addbutton = document.getElementById("addslides");
        previousButton = document.getElementById("previous");
        //        utils.addClass(previousButton, 'disabled');
        playbutton = document.getElementById("play");
        playPresentationButton = document.getElementById("playYes");
        nameAlreadyExistsOkButton = document.getElementById("nameAlreadyExistsOKButton");

        notPlayPresentationButton = document.getElementById("playNo");
        var returnbutton = document.getElementById("return");
        previousButton.href = './showplaylist.html';
        returnbutton.href = './index.html';

        returnbutton.addEventListener('click', openMenu, false);

        previousButton.addEventListener('click', openbuilder, false);
        //previousButton.addEventListener('click', openpage, false);
        slidesNumber = document.getElementById("slidesNumber");
        //POPUP SAVE
        saveallbutton = document.getElementById("saveallbutton");
        var cancelbutton = document.getElementById("cancelsave");

        savewindow = document.getElementById("savewindow");
        nameAlreadyExistsWindow = document.getElementById("nameAlreadyExistsWindow");
        playPresentationWindow = document.getElementById("playPresentation");

        pleaseEnterNameWindow = document.getElementById("pleaseEnterNameWindow");
        pleaseEnterNameWindowOKButton = document.getElementById("pleaseEnterNameWindowOKButton");

        allSlidesRequired = document.getElementById("allSlidesRequired");
        allSlidesRequiredOKButton = document.getElementById("allSlidesRequiredOKButton");

        sectionCannotBeDeselected = document.getElementById("sectionCannotBeDeselected");
        sectionCannotBeDeselectedOKButton = document.getElementById("sectionCannotBeDeselectedOKButton");

        slideCannotBeDeselected = document.getElementById("slideCannotBeDeselected");
        slideCannotBeDeselectedOKButton = document.getElementById("slideCannotBeDeselectedOKButton");

        addbutton.addEventListener('click', savechapters, false);
        //savebutton.addEventListener('click', openpopupsave, false);
        utils.addClass(savebutton, 'disabled');

        cancelbutton.addEventListener('click', cancelsave, false);
        saveallbutton.addEventListener('click', savepresentation, false);

        playbutton.addEventListener('click', openlink, false);
        playPresentationButton.addEventListener('click', openlink, false);
        notPlayPresentationButton.addEventListener('click', openMenu, false);
        nameAlreadyExistsOkButton.addEventListener('click', closeAlreadyExistsWindow, false);
        pleaseEnterNameWindowOKButton.addEventListener('click', closePleaseEnterNameWindow, false);
        allSlidesRequiredOKButton.addEventListener('click', closeAllSlidesRequiredWindow, false);
        sectionCannotBeDeselectedOKButton.addEventListener('click', closeSectionCannotBeDeselectedWindow, false);
        slideCannotBeDeselectedOKButton.addEventListener('click', closeSlideCannotBeDeselected, false);
        utils.addClass(playbutton, 'disabled');
        document.getElementById('errorwindow').addEventListener('click', closeerror, false);

        wrapper = document.getElementById("chapters");
        utils.addClass(wrapper, 'unsorted');
        list = document.createElement('ul');
        list.setAttribute('id', 'chapterlist');
        list.setAttribute('class', 'chapterlist');

        wrapper.appendChild(list);

        originalOrder = chapterarray.clone();

        for (var i = 0; i < chapterarray.length; i++) {
            if (chapterarray[i].substring(0, 1) != "-") {
                var forced = false;
                listitem = document.createElement('li');
                var ca = chapterarray[i];
                if (ca.substring(0, 1) == "+" || ca.substring(0, 1) == "!") {
                    if (ca.substring(0, 1) == "!") {
                        forced = true;
                    }
                    ca = ca.substring(1);
                }
                listitem.innerHTML = ca + '<span></span>';
                listitem.setAttribute('id', chapterarray[i]);
                listitem.code = chapterarray[i];
                listitem.sectioncode = selectionsarray[i][0];

                if (chapterarray[i].indexOf('home') != -1) {
                    listitem.innerHTML = '';
                    listitem.setAttribute('id', 'homebutton');
                    listitem.setAttribute('class', 'home');
                }
                list.appendChild(listitem);
                listitem.addEventListener('click', sortarray, false);
                listitemarray.push(listitem);
                if (forced) {
                    listitem.setAttribute('class', 'active');
                    listitem.removeEventListener('click', sortarray, false);
                    listitem.addEventListener('click', showmessage, false);
                    temparray.push(selectionsarray[i][0]);
                    order++;
                    numberOfSections = order;
                }
                else {
                    if (nextFreeSpace == null) {
                        nextFreeSpace = i;
                    }
                    temparray.push('-none-');
                }
            }
        }
        listitem = document.createElement('li');
        listitem.style.visibility = 'hidden';
        listitem.innerHTML = '<span>&nbsp</span>';
        list.appendChild(listitem);
    }

    function closeAlreadyExistsWindow() {
        utils.addClass(nameAlreadyExistsWindow, 'hide');
        utils.removeClass(nameAlreadyExistsWindow, 'show');
    }
    function closePleaseEnterNameWindow() {
        utils.addClass(pleaseEnterNameWindow, 'hide');
        utils.removeClass(pleaseEnterNameWindow, 'show');
    }
    function closeAllSlidesRequiredWindow() {
        utils.addClass(allSlidesRequired, 'hide');
        utils.removeClass(allSlidesRequired, 'show');
    }
    function closeSectionCannotBeDeselectedWindow() {
        utils.addClass(sectionCannotBeDeselected, 'hide');
        utils.removeClass(sectionCannotBeDeselected, 'show');
    }
    function closeSlideCannotBeDeselected() {
        utils.addClass(slideCannotBeDeselected, 'hide');
        utils.removeClass(slideCannotBeDeselected, 'show');
    }

    function openbuilder() {
        window.location.href = './showplaylist.html';
    }
    function openpage(e) {

        //addClass(element, 'active');
        window.location.href = window.location.href;
    }
    function openlink() {
        if (currentlink == undefined || currentlink == null) {
            return
        }
        var linkId = currentlink;
        window.location.href = '../../index.html?template=' + linkId;
    }

    function openMenu() {
        window.location.href = './index.html';
    }
    function savechapters() {
        //console.log('Temp array ' + temparray)
        if (temparray.length == 0) {
            errorpopup();
            errorstr = window.localisation.getString('builderPage', 'saveChapterFirst');
            document.getElementById('errordialog').innerHTML = '<h1>' + errorstr + '</h1>';
            return
        }
        previousButton.removeEventListener();
        previousButton.addEventListener('click', openpage, false);

        for (var i = 0; i < temparray.length; i++) {
            sectionsarray.push([temparray[i]]);

        }
        orderchapters();
        //document.getElementById('agendatitle').innerHTML = '<h1 style="margin-top:10px;">Create New Presentation</h1><p style="margin-top:10px;">Tap the thumbnails to select slides. If you tap the Previous button your selections will not be saved. We advise 15 slides maximum for a 10 minute presentation. Tap Next to save your presentation.</p>';
        $('#createNewPresentationText').html(window.localisation.getString('builderPage', 'createNewPresentationSlidesText'));
        utils.removeClass(savebutton, 'disabled');
        utils.addClass(addbutton, 'disabled');
        addbutton.removeEventListener('click', savechapters, false);
        savebutton.addEventListener('click', opensave, false);
        utils.removeClass(previousButton, 'disabled');
    }
    function errorpopup(e, errorstr) {
        utils.addClass(errorwindow, 'show');

    }
    function opensave() {
        var ok = true;
        for (var p = 0; p < tempslidesarray.length; p++) {
            var section = tempslidesarray[p];
            if (section.section.numberOfSlidesSelected < 1) {
                ok = false;
            }
        }

        if (!ok) {
            errorpopup();
            errorstr = window.localisation.getString('builderPage', 'selectSlidesFirst');
            document.getElementById('errorMessage').innerHTML = errorstr;
            return
        }
        utils.removeClass(savewindow, 'hide');
        utils.addClass(savewindow, 'show');

        slidesNumber.innerHTML = slidesaddedarray.length;
    }
    function savepresentation() {


        var presId = document.getElementById("title").value;
        presId = presId.replace(/ /g, '_');
        var currentLinks = localStorage.getItem('links');
        if (currentLinks != null) {
            var currentPresIDs = currentLinks.split(',');
            for (i = 0; i < currentPresIDs.length; i++) {
                var indexOfAnd = currentPresIDs[i].indexOf('&');
                var sub = currentPresIDs[i].substring(0, indexOfAnd);
                if (presId == sub) {
                    utils.removeClass(nameAlreadyExistsWindow, 'hide');
                    utils.addClass(nameAlreadyExistsWindow, 'show');
                    return;
                }
            }
        }
        if (presId == '') {
            utils.removeClass(pleaseEnterNameWindow, 'hide');
            utils.addClass(pleaseEnterNameWindow, 'show');
            return;
        }

        for (var i = 0; i < slidetoolsarray.length; i++) {
            slidesarray.unshift(slidetoolsarray[i]);
        }

        for (var i = 0; i < slidetoolsarray.length; i++) {
            slidesaddedarray.unshift(slidetoolsarray[i]);
        }

        for (var p = 0; p < tempslidesarray.length; p++) {
            if (tempslidesarray[p].clickstate == 0) {
                var sectionindex = slidesarray.indexOf(tempslidesarray[p].slides);
                var section = slidesarray[sectionindex];
                var slideindex = section.indexOf(tempslidesarray[p].code);
                section.splice(slideindex, 1);

                if (section.length == 0) {
                    slidesarray.splice(sectionindex, 1);
                    console.log('OUT ' + slidesarray);
                }
            }
        }
        if (slidesarray.length == 0) {
            return
        }
        console.log('Slides array ' + slidesarray);
        console.log('Pres name: ' + presId);
        //console.log('JSarray ' + jsarray);
        utils.removeClass(savewindow, 'show');
        //utils.removeClass(playbutton, 'disabled');
        utils.addClass(savebutton, 'disabled');
        $("#footer img#select").hide();
        $("#footer img#playBreadCrumb").show();
        savebutton.removeEventListener('click', opensave, false);
        commitvalues(presId, JSON.stringify(slidesarray));

        utils.removeClass(playPresentationWindow, 'hide');
        utils.addClass(playPresentationWindow, 'show');

        //        document.getElementById('agendatitle').innerHTML = '<h1>Build New Presentation</h1><p>Thank you click "PLAY".</p>';
        //        document.getElementById('scroller').innerHTML = '';

    }
    function commitvalues(presId, slidesarray, jsarray) {
        //CREATE LINKS ITEM
        var key = presId + model.appkey;

        if (localStorage.getItem('links')) {
            //Links holder
            var linkarray = new Array();
            var oldarrayvalues = localStorage.getItem('links');
            values = oldarrayvalues.split(","); //create an array of the values
            //Add existing values to link array
            for (var i = 0; i < values.length; i++) {
                linkarray.push(values[i]);
                if (key == values[i]) {
                    console.log('saving the same!!' + linkarray);
                    linkarray.splice(linkarray.indexOf(values[i]), 1);
                    console.log('OUT!!' + linkarray);
                }

            }
            //Add new value to link array
            linkarray.push(key);
            //Save array with new values
            localStorage.setItem('links', linkarray);

        } else {
            //Create local storage item if none
            localStorage.setItem('links', key);
            //alert("Clean and adding " + presId + " new")
        }

        //CREATE PRESENTATION ITEM
        var key = presId + model.appkey;
        console.log(key);

        localStorage.setItem(key, slidesarray);
        localStorage.setItem(key + 'menu', menuarray);
        //document.getElementById('tester').innerHTML = localStorage.getItem(presId) ? localStorage.getItem(presId) : "";
        currentlink = key;
    }

    function cancelsave() {
        utils.removeClass(savewindow, 'show');
        utils.addClass(savewindow, 'hide');
    }
    function closeerror() {
        utils.removeClass(errorwindow, 'show');
        document.getElementById('errorwindow').childNodes[0].innerHTML = '';
    }
    function orderchapters() {
        $("#return").html('');
        $("#return").attr('onclick', 'javascript: window.location="./builder.html"; return false;"');

        wrapper.removeChild(list);
        if (allowOrderingOfSections) {
            for (var j = 0; j < sectionsarray.length; j++) {
                for (var i = 0; i < listitemarray.length; i++) {
                    var item = listitemarray[i];
                    var fixed = false;
                    if (item.code.substring(0, 1) == "+" || item.code.substring(0, 1) == "!") {
                        fixed = true;
                    }
                    if (item.sectioncode == sectionsarray[j]) {
                        var newlist = document.createElement('ul');
                        item.setAttribute('class', 'chaptertitle');
                        menuarray.push(item.innerHTML);
                        slideswrapper.appendChild(newlist);
                        console.log('menu ' + menuarray);
                        newlist.appendChild(item);
                        addslides(sectionsarray[j], newlist, fixed);
                    }
                }
            }
        }
        else {
            for (var i = 0; i < listitemarray.length; i++) {
                var item = listitemarray[i];
                var fixed = false;
                if (item.code.substring(0, 1) == "+" || item.code.substring(0, 1) == "!") {
                    fixed = true;
                }

                //console.log('Compare ' + item.sectioncode);
                for (var j = 0; j < sectionsarray.length; j++) {
                    if (item.sectioncode == sectionsarray[j]) {
                        var newlist = document.createElement('ul');
                        item.setAttribute('class', 'chaptertitle');
                        menuarray.push(item.innerHTML);
                        slideswrapper.appendChild(newlist);
                        console.log('menu ' + menuarray);
                        newlist.appendChild(item);
                        addslides(sectionsarray[j], newlist, fixed);
                    }
                }
            }
        }
        utils.removeClass(wrapper, 'unsorted');
        utils.addClass(wrapper, 'sorted');
        utils.addClass(slideswrapper, 'sorted');
    }
    function addslides(section, sectionlist, fixed) {
        //console.log('slides array sections ' + section);
        this.sectionlist = sectionlist;
        checkvar = 'Checkbox';
        var b = $("#slidethumbs1").height();
        if (b < 400) {
            $("#downArrow").hide();
        } else {
            $("#downArrow").show();
        }
        if (numberOfSections > 4) {
            slideswrapper.style.width = '3072px';
            document.addEventListener('swipeleft', scrollleft);
            document.addEventListener('swiperight', scrollright);
            document.getElementById("arrowRight").style.display = "block";
            document.getElementById("arrowLeft").style.display = "none";
        }
        else {
            document.getElementById("arrowRight").style.display = "none";
            document.getElementById("arrowLeft").style.display = "none";
        }

        for (var i = 0; i < selectionsarray.length; i++) {
            console.log('slides array sections ' + section + ' ' + selectionsarray[i]);
            if (section == selectionsarray[i][0]) {
                var sectionslidesarray = selectionsarray[i];
                //console.log('Sections - get slides: ' + selectionsarray[i]);
                slidesarray.push(selectionsarray[i]);
                for (var j = 0; j < sectionslidesarray.length; j++) {
                    slidelistitem = document.createElement('li');
                    slidelistitem.innerHTML = '<span class="activeThumb"></span>';
                    //slidelistitem.innerHTML = '<img src="content/images/thumbnails/'+sectionslidesarray[j]+'.png">';

                    slidelistitem.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat 0px  0px, url(../../content/img/thumbs/' + sectionslidesarray[j] + '.jpg)  no-repeat 0px  0px;';
                    slidelistitem.slidethumb = sectionslidesarray[j];
                    slidelistitem.setAttribute('id', sectionslidesarray[j] + '_slide');
                    slidelistitem.setAttribute('class', 'slideitem');
                    var forced = false;
                    var ca = sectionslidesarray[j];
                    if (ca.substring(0, 1) == "+") {
                        forced = true;
                        ca = ca.substring(1);
                    }
                    slidelistitem.code = ca;
                    slidelistitem.slides = sectionslidesarray;
                    section.theOrder = 1;
                    section.numberOfSlidesSelected = 0;
                    slidelistitem.section = section;
                    slidelistitem.clickstate = 0;
                    slidelistitem.slidesorder = 0;
                    slidelistitem.addEventListener('click', sortslidesarray, false);
                    slidelistitem.sectionlist = this.sectionlist;
                    slidelistitem.originalOrder = j + 1;
                    if (fixed) {
                        slidelistitem.clickstate = 1;
                        slidelistitem.style.cssText = 'background: url(../../content/img/thumbs/' + ca + '.jpg)  no-repeat 0px  0px;';
                        //slidelistitem.style.cssText = 'background: url(images/agenda/Checkbox_flagged.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + ca + '.jpg)  no-repeat 3px  5px;';
                        slidesaddedarray.push(slidelistitem);
                        slidelistitem.setAttribute('class', 'activeThumb');
                        slidesadded = true;
                        section.theOrder++;
                        section.numberOfSlidesSelected++;
                        if (forced) {
                            slidelistitem.addEventListener('click', showslidemessage, false);
                            slidelistitem.removeEventListener('click', sortslidesarray, false);
                        }
                        else {
                            slidelistitem.addEventListener('click', sortslidesreset, false);
                        }
                    }
                    sectionlist.appendChild(slidelistitem);
                    tempslidesarray.push(slidelistitem);
                }
            }
        }

    }

    function sortslidesarray(e) {

        slidesaddedarray.push(e.target.code);
        slidesadded = true;
        e.target.clickstate = 1;
        checkvar = 'Checkbox_flagged';
        e.target.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat 0px  0px, url(../../content/img/thumbs/' + e.target.slidethumb + '.jpg)  no-repeat 0px  0px;';
        //e.target.style.cssText = 'background: url(images/agenda/' + checkvar + '.png) no-repeat -3px  -3px, url(../../content/img/thumbs/' + e.target.slidethumb + '.jpg)  no-repeat 3px  5px;';
        e.target.setAttribute('class', 'activeThumb');
        if (allowOrderingOfSlides) {
            var index = e.target.slides.indexOf(e.target.code);
            var mainindex = slidesarray.indexOf(e.target.slides);

            var listNode = e.target.sectionlist;
            console.log('Sort me: ' + e.target.code + ' in ' + slidesarray + ' at ' + e.target.slidesorder);

            e.target.slides.splice(index, 1);
            if (e.target.slides.length > e.target.section.theOrder) {
                e.target.slides.splice(e.target.section.theOrder, 0, e.target.code);
            }
            else {
                e.target.slides.push(e.target.code);
            }
            e.target.sectionlist.removeChild(e.target);
            e.target.sectionlist.insertBefore(e.target, listNode.childNodes[e.target.section.theOrder]);
            e.target.section.theOrder++;
            e.target.slidesorder++;
        }
        e.target.section.numberOfSlidesSelected++;
        e.target.removeEventListener('click', sortslidesarray, false);
        e.target.addEventListener('click', sortslidesreset, false);

    }

    function sortslidesreset(e) {
        var numberOfSlidesSelected = e.target.parentNode.section.theOrder - 1;
        slidesaddedarray.pop(e.target.code);
        checkvar = 'Checkbox';
        e.target.style.cssText = ' url(../../content/img/thumbs/' + e.target.slidethumb + '.jpg)  no-repeat 0px  0px;';
        e.target.parentNode.clickstate = 0;
        e.target.parentNode.setAttribute('class', 'slideitem');
        var index = e.target.parentNode.slides.indexOf(e.target.code);
        var listNode = e.target.parentNode.sectionlist;
        console.log('slides array remove' + listNode.code + ' at index ' + e.target.parentNode.slidesorder)
        if (allowOrderingOfSlides) {
            listNode.removeChild(e.target.parentNode);
            var lis = listNode.childNodes;
            var found = false;
            if (e.target.parentNode.originalOrder == 1) {
                listNode.insertBefore(e.target.parentNode, lis[numberOfSlidesSelected]);
                found = true;
            }
            else {
                for (i = numberOfSlidesSelected - 1; i < lis.length; i++) {
                    if (e.target.parentNode.originalOrder > lis[i].originalOrder) {
                        if ((i + 1) < lis.length) {
                            listNode.insertBefore(e.target.parentNode, lis[i + 1]);
                        }
                        else {
                            listNode.appendChild(e.target.parentNode);
                        }
                        found = true;
                    }
                }
            }
            if (!found) {
                listNode.insertBefore(e.target.parentNode, lis[numberOfSlidesSelected]);
            }
            e.target.parentNode.section.theOrder--;
            e.target.parentNode.slidesorder--;
        }
        e.target.parentNode.section.numberOfSlidesSelected--;
        e.target.parentNode.addEventListener('click', sortslidesarray, false);
        e.target.parentNode.removeEventListener('click', sortslidesreset, false);
    }


    function sortarray(e) {
        var index = chapterarray.indexOf(e.target.code);
        utils.addClass(e.target, 'active');
        if (allowOrderingOfSections) {
            temparray.push(e.target.sectioncode);
        }
        else {
            temparray.push(e.target.sectioncode);
        }

        if (e.target.code.substring(0, 1) == "+") {
            utils.removeClass(allSlidesRequired, 'hide');
            utils.addClass(allSlidesRequired, 'show');
        }

        if (allowOrderingOfSections) {
            chapterarray.splice(index, 1);
            chapterarray.unshift(e.target.code);

            list.removeChild(e.target);
            list.insertBefore(e.target, list.childNodes[order]);

            order++;
        }
        numberOfSections = order;
        e.target.removeEventListener('click', sortarray, false);
        e.target.addEventListener('click', sortreset, false);
    }
    function sortreset(e) {

        var index = temparray.indexOf(e.target.sectioncode);
        temparray[index] = "-none";
        nextFreeSpace = index;
        console.log('Sections array remove' + e.target.code + ' at index ' + index)

        utils.removeClass(e.target, 'active');
        e.target.addEventListener('click', sortarray, false);
        e.target.removeEventListener('click', sortreset, false);
        list.removeChild(e.target);
        var currentArray = new Array();
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == false) {
                currentArray.push(list.childNodes[index].code);
            }
        });

        var newPosition = getCurrentPosition(originalOrder, currentArray, e.target.code);
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == true && index <= newPosition) {
                newPosition++;
            }
        });
        var oneAhead = list.childNodes[newPosition];
        list.insertBefore(e.target, oneAhead);

        nextFreeSpace = 0;
        order = 0;
        $('#chapterlist li').each(function (index) {
            if ($(this).hasClass('active') == true) {
                nextFreeSpace++;
                order++;
            }
        });
        numberOfSections = order;
        //order--;

    }

    function getCurrentPosition(original, current, code) {
        var originalPosition;
        for (i = 0; i < original.length; i++) {
            if (original[i] == code) {
                originalPosition = i;
            }
        }
        if (originalPosition == 0) {
            return 0;
        }
        var codesBefore = new Array();
        for (i = 0; i < originalPosition; i++) {
            codesBefore.push(original[i]);
        }
        var newPosition = 0;
        var shouldBreak = false;
        for (i = codesBefore.length - 1; i > -1; i--) {
            if (shouldBreak) {
                break;
            }
            for (j = current.length - 1; j > -1; j--) {
                //alert("a: " + codesBefore[i] + ", b: " + current[j]);
                if (current[j] == codesBefore[i]) {
                    newPosition = j + 1;
                    shouldBreak = true;
                    break;
                }
            }
        }
        return newPosition;
    }

    function showmessage() {
        utils.removeClass(sectionCannotBeDeselected, 'hide');
        utils.addClass(sectionCannotBeDeselected, 'show');
    }

    function showslidemessage() {
        utils.removeClass(slideCannotBeDeselected, 'hide');
        utils.addClass(slideCannotBeDeselected, 'show');
    }

    var page = 1;


    function scrollleft() {
        if (page == 1 && numberOfSections > 4) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-1024px,0px,0px);';
            page++;
            if (numberOfSections > 8) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "block";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
        else if (page == 2 && numberOfSections > 8) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-2048px,0px,0px);';
            page++;
            if (numberOfSections > 12) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "none";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
    }
    function scrollright() {
        if (page == 2) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(0px,0px,0px);';
            page--;
            if (numberOfSections > 4) {
                document.getElementById("arrowRight").style.display = "block";
                document.getElementById("arrowLeft").style.display = "none";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
                document.getElementById("arrowLeft").style.display = "block";
            }
        }
        else if (page == 3) {
            document.getElementById('scroller').style.cssText = '-webkit-transform:translate3d(-1024px,0px,0px);';
            page--;
            if (numberOfSections > 8) {
                document.getElementById("arrowRight").style.display = "block";
            }
            else {
                document.getElementById("arrowRight").style.display = "none";
            }
        }
    }

});	    
	    
