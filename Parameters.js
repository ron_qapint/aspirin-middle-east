﻿//Do not touch this parameter. Used for workflow
var firstLoaded = true; 

//Initial load time for presentation
var redirectTimeout = 1000;

//Language selection file (languages located in modules/localisation/languages
var language = "english.xml";

//Set the page titles
document.title = "Aspirin ACS";

//Menu settings - set width of menu here;
var menuWidth = 1000;

//Thumbnail settings - whether thumbnail menu shows when section clicked on
var showThumbnailsOnMenuClick = false;
//This needs further work. Please leave true
var showThumbnailIfOnlyOneSlide = true;

//Builder settings - whether slides and sections are in forced orders or not
var allowOrderingOfSections = true;
var allowOrderingOfSlides = true;
//Whether to show builder in menu
var showBuilder = true;

//This app key needs to be in the format '&ProjectName' and must be unique between projects
var appkey = '&ACS';

//This decides whether to show full presentation in dropdown list on home page
var showFullPresentationInDropDown = true;

//Allow swiping up and down at end of slide show to navigate between sections
var allowUpDownSwipeSectionChange = true;

//This is the list of chapters for the presentation
//Prefixing a chapter with '!' will force the section plus all its slides to be automatically chosen
//Prefixing a chapter with '+' will force all slides in that section to be chosen automatically. They cannot be deselected.
//Prefixing a chapter with a '-' will make the chapter unavailable in the builder

//var chapterarray = ['!Home', 'Questions', 'SmPC'];
var chapterarray = ['!Home', 'Questions', 'Efficacy', 'Guidelines Recommendations', 'Safety', 'Aspirin<sup>®</sup> Protect', 'SmPC'];

//These are the slides that belong to each chapter above.
//var selectionsarray = [['splash', 'a_unique_pill', 'dosing', 'real_world_needs'], ['compliance', 'one_pill_per_day', 'compliance_with_24_7', 'safety', 'safety2', 'safety3'], ['efficacy', 'ovulation_inhibition', 'ovulation_inhibition2', 'ovulation_inhibition3', 'ovulation_inhibition4', 'ovulation_inhibition5', 'real_life_data', 'real_life_data2', 'real_life_data3', 'real_life_data4', 'real_life_data5', 'efficacy_in_adolescents', 'efficacy_in_adolescents2', 'efficacy_in_adolescents3', 'efficacy_in_adolescents4'], ['benefits', 'drospirenone', 'antimineralocorticoid_activity', 'antimineralocorticoid_activity2', 'antiandrogenic_activity', 'acne', 'acne2', 'symptoms_21_7_vs_24_7', 'symptoms_21_7_vs_24_72'], ['satisfaction', 'world_users', 'bleeding', 'bleeding2', 'bleeding3', 'bleeding4', 'patient_discontinuation', 'well_being', 'well_being2']];
var selectionsarray = [['+splash'],
                       ['questions_contents'],
                       ['questions_why_is_aspirin_used_acutely1', 'questions_why_is_aspirin_used_acutely2', 'questions_why_is_aspirin_used_acutely3', 'questions_why_is_aspirin_used_acutely4', 'questions_which_patients_benefit', 'questions_patients_undergoing_revascularization'],
                       ['questions_current_position_of_aspirin', 'questions_recommendations_in_acute_treatment', 'questions_recommendations_prevent_recurrent_events', 'questions_aspirin_for_noncardiac_surgery'],
                       ['questions_risk_factors_for_bleeding', 'questions_strategies_to_reduce_bleeding'],
                       ['questions_what_makes_aspirin_different'],
                       ['spc']
                       ];



//var selectionsarray = [['+splash'],
//                       ['questions_contents', 'questions_which_patients_benefit'],
//                       ['questions_patients_undergoing_revascularization', 'questions_strategies_to_reduce_bleeding'],
//                       ['questions_current_position_of_aspirin', 'questions_why_is_aspirin_used_acutely1', 'questions_why_is_aspirin_used_acutely2', 'questions_why_is_aspirin_used_acutely3', 'questions_why_is_aspirin_used_acutely4', 'questions_recommendations_in_acute_treatment', 'questions_recommendations_in_acute_treatment_popup', 'questions_recommendations_prevent_recurrent_events'],
//                       ['questions_risk_factors_for_bleeding', 'questions_aspirin_for_noncardiac_surgery'],
//                       ['questions_what_makes_aspirin_different'],
//                       ['spc']
//                       ];