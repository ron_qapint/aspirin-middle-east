﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_what_makes_aspirin_different_popup = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref27").clone().appendTo(".RefPopup");
            $("li#ref28").clone().appendTo(".RefPopup");

            $('#aniQdif-popup-an-scene-0').addClass('run t-0');

            $('#questions_what_makes_aspirin_different_popup').find('h3').click(function () {
                $('#aniQdif-popup-an-scene-0').removeClass('t-0');
                $('#aniQdif-popup-an-obj-7').css({ '-webkit-transform': 'translate3d(131px,116px,0px)', 'opacity': '1' });
            });

        },
        onExit: function (ele) {
            $(".RefPopup").empty();
            $("li#ref28").clone().appendTo(".RefPopup");

            $('#aniQdif-popup-an-scene-0').removeClass('run t-0');
        }
    };
}); 




