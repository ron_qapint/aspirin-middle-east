﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_why_is_aspirin_used_acutely4 = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref04").clone().appendTo(".RefPopup");
            $("li#ref05").clone().appendTo(".RefPopup");
            $("li#ref06").clone().appendTo(".RefPopup");

            $('#animQ4-an-scene-0').addClass('run t-0');

            $('#questions_why_is_aspirin_used_acutely4').find('h1').click(function () {
                $('#animQ4-an-scene-0').removeClass('t-0');
                $('#animQ4-an-obj-19').removeClass('delay');
            });
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#animQ4-an-obj-19').addClass('delay');
            $('#animQ4-an-scene-0').removeClass('run t-0');
        }
    };
}); 




