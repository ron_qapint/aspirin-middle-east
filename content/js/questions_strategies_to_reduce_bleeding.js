﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_strategies_to_reduce_bleeding = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref15").clone().appendTo(".RefPopup");
            $("li#ref16").clone().appendTo(".RefPopup");
            $("li#ref17").clone().appendTo(".RefPopup");
            $("li#ref18").clone().appendTo(".RefPopup");
            $("li#ref22").clone().appendTo(".RefPopup");

            $('#questions_strategies_to_reduce_bleeding').find('.btn-options').unbind('click').bind('click', function () {
                $(this).removeClass('glowGreen').addClass('active').next('.answer-box').addClass('active');
            });
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#questions_strategies_to_reduce_bleeding').find('.btn-options').unbind('click').addClass('glowGreen').removeClass('active');
            $('#questions_strategies_to_reduce_bleeding').find('.answer-box').removeClass('active');
        }
    };
}); 




