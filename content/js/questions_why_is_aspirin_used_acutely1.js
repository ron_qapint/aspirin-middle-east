﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_why_is_aspirin_used_acutely1 = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();

            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref02").clone().appendTo(".RefPopup");


            removeDuplicates();   // fix for question contents; do not remove


            $('.btn-green-big').bind('tap click', function () {
                if (!$(this).hasClass('active')) {
                    $(this).removeClass('glowGreen').addClass('active').siblings().removeClass('active').addClass('glowGreen');
                    if ($(this).hasClass('first')) {
                        $('.button-text-box').find('.p1').removeClass('no-opacity').addClass('opacity').siblings().removeClass('opacity').addClass('no-opacity');
                        $('.circle-box').find('#btn-label-1').addClass('show').siblings().removeClass('show');
                    }
                    else {
                        $('.button-text-box').find('.p2').removeClass('no-opacity').addClass('opacity').siblings().removeClass('opacity').addClass('no-opacity');
                        $('.circle-box').find('#btn-label-2').addClass('show').siblings().removeClass('show');
                    }
                }
            });
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#questions_why_is_aspirin_used_acutely1').find('.btn-green-big').removeClass('active').addClass('glowGreen');
            $('#questions_why_is_aspirin_used_acutely1').find('.big-circle span').removeClass('show');
            $('.button-text-box').find('p').removeClass('opacity').addClass('no-opacity');
        }
    };
}); 




