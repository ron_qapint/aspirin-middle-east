﻿// JavaScript Document

var aniQ6_1, aniQ6_2, aniQ6_3;
var aniQ6counter = 0;

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_patients_undergoing_revascularization = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref13").clone().appendTo(".RefPopup");
            $("li#ref14").clone().appendTo(".RefPopup");

            $('#aniQ6-1-an-scene-0, #aniQ6-2-an-scene-0, #aniQ6-3-an-scene-0').removeClass('run t-0');
            if ($('#aniQ6-1-an-scene-0').hasClass('run') || $('#aniQ6-2-an-scene-0').hasClass('run') || $('#aniQ6-3-an-scene-0').hasClass('run')) {
                $('#questions_patients_undergoing_revascularization').find('.animation-box').addClass('boxUp');
            }


            function animateQ6(btn, time) {
                if ($(btn).hasClass('btn-1')) {
                    aniQ6_1 = setTimeout(function () {
                        $('#aniQ6-1-an-scene-0').addClass('run t-0');
                    }, time);
                }

                else if ($(btn).hasClass('btn-2')) {
                    aniQ6_2 = setTimeout(function () {
                        $('#aniQ6-2-an-scene-0').addClass('run t-0');
                    }, time);
                }

                else if ($(btn).hasClass('btn-3')) {
                    aniQ6_3 = setTimeout(function () {
                        $('#aniQ6-3-an-scene-0').addClass('run t-0');
                    }, time);
                }
            }


            $('#questions_patients_undergoing_revascularization').find('.btn-options').click(function () {
                $(this).removeClass('glowGreen').addClass('active');

                if (aniQ6counter == 0) {
                    $('#questions_patients_undergoing_revascularization').find('.animation-box').addClass('boxUp');

                    animateQ6($(this), 1500);
                    aniQ6counter++;
                }
                else {
                    animateQ6($(this), 500);
                }

            });
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#aniQ6-1-an-scene-0, #aniQ6-2-an-scene-0, #aniQ6-3-an-scene-0').removeClass('run t-0');
            $('.btn-options').removeClass('active').addClass('glowGreen');
            $('.animation-box').removeClass('boxUp');

            clearTimeout(aniQ6_1);
            clearTimeout(aniQ6_2);
            clearTimeout(aniQ6_3);
            aniQ6counter = 0;
        }
    };
}); 




