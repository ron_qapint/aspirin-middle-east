﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_why_is_aspirin_used_acutely2 = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref03").clone().appendTo(".RefPopup");

            $('#animQ2-an-scene-0').addClass('run t-0');
            $('#questions_why_is_aspirin_used_acutely2').find('h1').click(function () {
                console.log('y');
                $('#animQ2-an-scene-0').removeClass('t-0');
                $('#animQ2-an-obj-4, #animQ2-an-obj-7').hide();
                $('.animation-box').find('li').removeClass('anim-elem');
            });

        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#animQ2-an-obj-4, #animQ2-an-obj-7').show();
            $('#animQ2-an-scene-0').removeClass('run t-0');
            $('.animation-box').find('li').addClass('anim-elem');
        }
    };
}); 




