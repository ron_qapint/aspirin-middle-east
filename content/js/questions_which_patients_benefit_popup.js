﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_which_patients_benefit_popup = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref07").clone().appendTo(".RefPopup");
            $("li#ref09").clone().appendTo(".RefPopup");
            $("li#ref10").clone().appendTo(".RefPopup");
            $("li#ref11").clone().appendTo(".RefPopup");
            $("li#ref12").clone().appendTo(".RefPopup");

            $('#aniQ5popup-an-scene-0').addClass('run t-0');

            $('#questions_which_patients_benefit_popup').find('h3').click(function () {
                $('#aniQ5popup-an-scene-0').removeClass('t-0');
            });
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").empty();
            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref03").clone().appendTo(".RefPopup");
            $("li#ref07").clone().appendTo(".RefPopup");
            $("li#ref08").clone().appendTo(".RefPopup");
            $("li#ref09").clone().appendTo(".RefPopup");
            $("li#ref10").clone().appendTo(".RefPopup");
            $("li#ref11").clone().appendTo(".RefPopup");

            $('#aniQ5popup-an-scene-0').removeClass('run t-0');

        }
    };
}); 




