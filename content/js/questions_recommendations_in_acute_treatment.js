﻿// JavaScript Document


document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_recommendations_in_acute_treatment = {
        elements: {

        },
        onEnter: function (ele) {


            $('#questions_recommendations_in_acute_treatment').find('.acc-header').unbind('click').bind('click', function () {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').next('.acc-content').css('display', 'none').slideDown(200).end()
                        .closest('.acc-el').siblings().each(function () {
                            $(this).find('.acc-header').removeClass('active').next('.acc-content').slideUp(200);
                        });
                }
                else {
                    $(this).removeClass('active').next('.acc-content').slideUp(200);
                }
            });

            $(".RefPopup").empty();           
            $("li#ref15").clone().appendTo(".RefPopup");
            $("li#ref16").clone().appendTo(".RefPopup");
            $("li#ref17").clone().appendTo(".RefPopup");
            $("li#ref18").clone().appendTo(".RefPopup");

        },

        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();
            

            $('.accordion').find('.acc-header').each(function () {
                $(this).unbind('click').removeClass('active').next('.acc-content').slideUp(20);
            });
        }
    };
}); 




