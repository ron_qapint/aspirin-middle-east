﻿function refsLoaded() {
    $('.refs .tab').click(function () {
        $(this).addClass('active').siblings('.tab').removeClass('active');
        $('.refs').find('.ref').fadeOut(100).eq($(this).index()).fadeIn(100);
    });

    $('#closeRefs2').click(function () {
        $('#lightboxOverlay').fadeOut(600);
        $(".RefPopup").children().remove();
        $(this).parent().fadeOut(600, function () {
            $(this).find('.popup-box').remove();
            $(this).removeAttr('style');
            app.scroller.enableAll();
            $('.handle').show();
        });

    });

}