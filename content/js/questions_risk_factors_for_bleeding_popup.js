﻿// JavaScript Document

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_risk_factors_for_bleeding_popup = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref26").clone().appendTo(".RefPopup");

            $('#aniQrisk-popup-an-scene-0').addClass('run t-0');
            $('#questions_risk_factors_for_bleeding_popup').find('#aniQrisk-popup-an-obj-1').stop(true, true).delay(800).fadeIn(100);
            $('#questions_risk_factors_for_bleeding_popup').find('.aspirin-highlight').stop(true, true).delay(6500).fadeIn(500);

            $('#questions_risk_factors_for_bleeding_popup').find('h3').click(function () {
                $('#aniQrisk-popup-an-scene-0').removeClass('t-0');
                $('#questions_risk_factors_for_bleeding_popup').find('#aniQrisk-popup-an-obj-1').stop(true, true).fadeIn(10);
                $('#questions_risk_factors_for_bleeding_popup').find('.aspirin-highlight').stop(true, true).fadeIn(500);
                $('#aniQrisk-popup-an-obj-2').hide();
            });
        },
        onExit: function (ele) {
            $(".RefPopup").empty();
            $("li#ref25").clone().appendTo(".RefPopup");

            $('#aniQrisk-popup-an-scene-0').removeClass('run t-0');
            $('#questions_risk_factors_for_bleeding_popup').find('.aspirin-highlight, #aniQrisk-popup-an-obj-1').hide();
            $('#aniQrisk-popup-an-obj-2').show();
        }
    };
}); 




