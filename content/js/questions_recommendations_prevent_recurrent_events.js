﻿// JavaScript Document


document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_recommendations_prevent_recurrent_events = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref15").clone().appendTo(".RefPopup");
            $("li#ref16").clone().appendTo(".RefPopup");
            $("li#ref17").clone().appendTo(".RefPopup");
            $("li#ref18").clone().appendTo(".RefPopup");
            $("li#ref19").clone().appendTo(".RefPopup");
            $("li#ref20").clone().appendTo(".RefPopup");
            $("li#ref21").clone().appendTo(".RefPopup");

            $('#questions_recommendations_prevent_recurrent_events').find('.acc-header').unbind('click').bind('click', function () {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').next('.acc-content').slideDown(200).end()
                        .closest('.acc-el').siblings().each(function () {
                            $(this).find('.acc-header').removeClass('active').next('.acc-content').slideUp(200);
                        });
                }
                else {
                    $(this).removeClass('active').next('.acc-content').slideUp(200);
                }
            });

        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('.accordion').find('h3').unbind('click')

            $('.accordion').find('.acc-header').each(function () {
                $(this).unbind('click').removeClass('active').next('.acc-content').slideUp(20);
            });
        }
    };
});


