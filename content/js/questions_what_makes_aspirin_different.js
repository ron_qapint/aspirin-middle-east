﻿// JavaScript Document

var addGlow;

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_what_makes_aspirin_different = {
        elements: {

        },
        onEnter: function (ele) {

            removeDuplicates();     // fix for question contents; do not remove 

            $('#questions_what_makes_aspirin_different').find('.row, .learn-more').addClass('active');

            clearTimeout(addGlow);
            addGlow = setTimeout(function () {
                $('#questions_what_makes_aspirin_different').find('.learn-more.btn2').removeClass('active').addClass('shown glowGreen');
            }, 5000);

            $(".RefPopup").empty();
            $("li#ref28").clone().appendTo(".RefPopup");
        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            clearTimeout(addGlow);

            $('#questions_what_makes_aspirin_different').find('.learn-more.btn2').removeClass('show');

            $('#questions_what_makes_aspirin_different').find('.row').removeClass('active');
            $('#questions_what_makes_aspirin_different').find('.learn-more.btn2').removeClass('shown glowGreen active');

            
        }
    };
}); 




