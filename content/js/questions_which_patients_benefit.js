﻿// JavaScript Document

var aniQ5left, aniQ5right;
var aniQ5counter = 0;

document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_which_patients_benefit = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref01").clone().appendTo(".RefPopup");
            $("li#ref03").clone().appendTo(".RefPopup");
            $("li#ref07").clone().appendTo(".RefPopup");
            $("li#ref08").clone().appendTo(".RefPopup");
            $("li#ref09").clone().appendTo(".RefPopup");
            $("li#ref10").clone().appendTo(".RefPopup");
            $("li#ref11").clone().appendTo(".RefPopup");

            function animateQ5(btn, time) {
                if ($(btn).hasClass('left-btn')) {
                    aniQ5left = setTimeout(function () {
                        $('#aniQ5left-an-scene-0').addClass('run t-0');
                    }, time);
                }

                else if ($(btn).hasClass('right-btn')) {
                    aniQ5right = setTimeout(function () {
                        $('#aniQ5right-an-scene-0').addClass('run t-0');
                    }, time);
                }
            }

            $('#questions_which_patients_benefit').find('.btn-options').click(function () {
                $(this).removeClass('glowGreen').addClass('active');

                if (aniQ5counter == 0) {
                    $('#questions_which_patients_benefit').find('.animation-box').addClass('boxUp');

                    animateQ5($(this), 1500);

                    $('.learn-box').addClass('anim-elem');
                    aniQ5counter++;
                }

                else if (aniQ5counter > 0) {
                    animateQ5($(this), 500);
                }

            });

        },
        onExit: function (ele) {
            $("#openRefs").css("display", "block");
            $(".RefPopup").children().remove();

            $('#aniQ5left-an-scene-0, #aniQ5right-an-scene-0').removeClass('run t-0');
            $('.btn-options').removeClass('active').addClass('glowGreen');
            $('.animation-box').removeClass('boxUp');

            $('.learn-box').removeClass('anim-elem');
            clearTimeout(aniQ5left);
            clearTimeout(aniQ5right);
            aniQ5counter = 0;

        }
    };
}); 




