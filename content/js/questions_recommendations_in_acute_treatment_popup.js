﻿// JavaScript Document


document.addEventListener('presentationInit', function () {
    var slide = app.slide.questions_recommendations_in_acute_treatment_popup = {
        elements: {

        },
        onEnter: function (ele) {
            $(".RefPopup").empty();
            $("li#ref15").clone().appendTo(".RefPopup");
            $("li#ref16").clone().appendTo(".RefPopup");
            $("li#ref17").clone().appendTo(".RefPopup");
            $("li#ref18").clone().appendTo(".RefPopup");

            $('#questions_recommendations_in_acute_treatment_popup .accordion').find('.acc-header').unbind('click').bind('click', function () {
                if (!$(this).hasClass('active')) {
                    $(this).addClass('active').next('.acc-content').slideDown(200).end()
                        .closest('.acc-el').siblings().each(function () {
                            $(this).find('.acc-header').removeClass('active').next('.acc-content').slideUp(200);
                        });
                }
                else {
                    $(this).removeClass('active').next('.acc-content').slideUp(200);
                }
            });

        },

        onExit: function (ele) {
            $(".RefPopup").empty();
            $("li#ref15").clone().appendTo(".RefPopup");
            $("li#ref16").clone().appendTo(".RefPopup");
            $("li#ref17").clone().appendTo(".RefPopup");
            $("li#ref18").clone().appendTo(".RefPopup");

            $('.accordion').find('.acc-header').each(function () {
                $(this).unbind('click').removeClass('active').next('.acc-content').slideUp(20);
            });
        }
    };
}); 




